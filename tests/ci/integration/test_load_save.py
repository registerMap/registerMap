#
# Copyright (c) 2019 Russell Smiley
#
# This file is part of registerMap.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

# import the top-level module to test that load, save are importable at this level
import registermap


class TestLoad:
    def test_load(self, mocker):
        mock_filename = "some/file"
        mock_yaml_load = mocker.patch("registermap.registerMap.load_yaml_data")
        mock_from = mocker.patch(
            "registermap.registerMap.RegisterMap.from_yamlData"
        )
        loaded_map = registermap.load(mock_filename)

        registermap_instance = mock_from.return_value

        assert loaded_map == registermap_instance
        mock_from.assert_called_once()

        mock_yaml_load.assert_called_once_with(mock_filename)


class TestSave:
    def test_save(self, mocker):
        mock_filename = "some/file"
        mock_yaml_save = mocker.patch("registermap.registerMap.save_yaml_data")
        mock_map = mocker.create_autospec(registermap.RegisterMap)

        registermap.save(mock_filename, mock_map)

        mock_map.to_yamlData.assert_called_once()
        mock_yaml_save.assert_called_once_with(
            mock_filename, mock_map.to_yamlData.return_value
        )
