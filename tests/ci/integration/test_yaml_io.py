#  Copyright (c) 2017 Russell Smiley
#
#  This file is part of registermap.
#
#  You should have received a copy of the GNU General Public License
#  along with registermap.  If not, see <http://www.gnu.org/licenses/>.

import logging

import pytest

from registermap.registerMap import ParseError, RegisterMap

log = logging.getLogger(__name__)


class TestYamlLoadSave:
    def test_encode_decode(self):
        m = RegisterMap()

        m.memory.addressBits = 48
        m.memory.baseAddress = 0x1000
        m.memory.memoryUnitBits = 16
        m.memory.pageSize = 128
        m["description"] = "some description"
        m["summary"] = "a summary"
        self._create_sample_module(m, "m1")

        encoded_yaml_data = m.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_map = RegisterMap.from_yamlData(encoded_yaml_data)

        assert decoded_map.memory.addressBits == m.memory.addressBits
        assert decoded_map.memory.baseAddress == m.memory.baseAddress
        assert decoded_map.memory.memoryUnitBits == m.memory.memoryUnitBits
        assert decoded_map.memory.pageSize == m.memory.pageSize
        assert decoded_map["description"] == m["description"]
        assert decoded_map["summary"] == m["summary"]

        assert len(decoded_map["modules"]) == len(m["modules"])
        assert decoded_map["modules"]["m1"]["name"] == "m1"

    def _create_sample_module(self, this_map, module_name):
        sample_module = this_map.addModule(module_name)

        register_name = "r1"
        sample_module.addRegister(register_name)

        sample_module["registers"][register_name]["constraints"][
            "fixedAddress"
        ] = 0x1010
        sample_module["registers"][register_name][
            "description"
        ] = "some description"
        sample_module["registers"][register_name]["mode"] = "ro"
        sample_module["registers"][register_name]["public"] = False
        sample_module["registers"][register_name]["summary"] = "a summary"

        sample_module["registers"][register_name].addField("f1", [3, 5], (3, 5))
        sample_module["registers"][register_name].addField("f2", [7, 7], (7, 7))

    def test_from_bad_yaml_data_raises(self):
        yaml_data = {"mode": "ro"}

        with pytest.raises(
            ParseError, match="^RegisterMap is not defined in yaml data"
        ):
            RegisterMap.from_yamlData(yaml_data, optional=False)

    def test_optional_yaml_data(self):
        yaml_data = {"mode": "ro"}

        m = RegisterMap.from_yamlData(yaml_data, optional=True)

        assert m.memory.addressBits == 32
        assert m.memory.baseAddress == 0
        assert m.memory.memoryUnitBits == 8
        assert m.memory.pageSize is None
        assert len(m["modules"]) == 0

    def test_synchronization(self):
        this_map = RegisterMap()

        this_map.memory.addressBits = 48
        this_map.memory.baseAddress = 0
        this_map.memory.memoryUnitBits = 8
        this_map.memory.pageSize = None

        m1 = this_map.addModule("m1")
        r1 = m1.addRegister("r1")
        m2 = this_map.addModule("m2")
        r2 = m2.addRegister("r2")
        r2.addField("f2", [3, 10], (3, 10))
        r2["constraints"]["fixedAddress"] = 0x15

        encoded_yaml_data = this_map.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_map = RegisterMap.from_yamlData(encoded_yaml_data)

        # Changing the base address for each map must mean that elements end up
        # with the same addresses.
        this_map.memory.baseAddress = 0x10
        decoded_map.memory.baseAddress = 0x10

        # Existing registers and modules must reflect the base address change.
        assert decoded_map["modules"]["m1"].baseAddress == m1.baseAddress
        assert (
            decoded_map["modules"]["m1"]["registers"]["r1"].startAddress
            == r1.startAddress
        )
        assert decoded_map["modules"]["m2"].baseAddress == m2.baseAddress
        assert (
            decoded_map["modules"]["m2"]["registers"]["r2"].startAddress
            == r2.startAddress
        )
