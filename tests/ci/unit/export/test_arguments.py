#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.export.arguments import parseArguments


class TestExporterArgumentParser:
    def test_register_map_file_path(self):
        input_value = [
            "path/registerMapFile",
            "c++",
            "output/path",
        ]

        options_under_test = parseArguments(input_value)

        assert input_value[0] == options_under_test.registerMapFile

    def test_default_register_map_name(self):
        input_value = [
            "path/registerMapFile",
            "c++",
            "output/path",
        ]

        options_under_test = parseArguments(input_value)

        assert "registermap" == options_under_test.registerMapName

    def test_missing_language_raises(self):
        input_value = [
            "path/registerMapFile",
        ]

        with pytest.raises(RuntimeError, match="^Language must be specified"):
            parseArguments(input_value)

    def test_long_license_file_option(self):
        input_value = [
            "path/registerMapFile",
            "--license-file",
            "license/path",
            "c++",
            "output/path",
        ]

        options_under_test = parseArguments(input_value)

        assert input_value[2] == options_under_test.licenseFile

    def test_short_license_file_option(self):
        input_value = [
            "path/registerMapFile",
            "-l",
            "license/path",
            "c++",
            "output/path",
        ]

        options_under_test = parseArguments(input_value)

        assert input_value[2] == options_under_test.licenseFile

    def test_long_register_map_name_option(self):
        input_value = [
            "path/registerMapFile",
            "--registermap-name",
            "someName",
            "c++",
            "output/path",
        ]

        options_under_test = parseArguments(input_value)

        assert input_value[2] == options_under_test.registerMapName

    def test_short_register_map_name_option(self):
        input_value = [
            "path/registerMapFile",
            "-n",
            "someName",
            "c++",
            "output/path",
        ]

        options_under_test = parseArguments(input_value)

        assert input_value[2] == options_under_test.registerMapName
