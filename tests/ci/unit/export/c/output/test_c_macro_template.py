#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

import copy
import os

import jinja2
import pytest

import registermap.export.base.template
from registermap.export.c.output.macro import (
    MACRO_TEMPLATE_CONFIGURATION,
    MacroTemplates,
)
from tests.ci.unit.export.common_cpp_c.output import do_template_test

CREATE_DIRECTORY_MODULE_PATH = (
    "registermap.export.c.output.macro.MacroTemplates.createDirectory"
)
OPEN_MODULE_PATH = "registermap.export.commonCppC.output.macro.open"


class TestMacroTemplate:
    @pytest.fixture()
    def elements_context(self):
        self.expected_name = "someName"
        self.include_directory = "include/path"
        self.source_directory = "source/path"

    def test_defaults(self, elements_context):
        expected_macro_directory = os.path.join(self.include_directory, "macro")
        expected_template_directory = os.path.join("templates", "macro")

        paths = MacroTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.sourceDirectory = self.source_directory

        output_under_test = MacroTemplates(paths, self.expected_name)

        assert self.expected_name == output_under_test.registerMapName
        assert (
            expected_template_directory == output_under_test.templateDirectory
        )
        assert expected_macro_directory == output_under_test.macroDirectory

    def test_initial_created_files(self, elements_context, mocker):
        paths = MacroTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.sourceDirectory = self.source_directory

        output_under_test = MacroTemplates(paths, self.expected_name)
        mock_template = mocker.create_autospec(jinja2.Template)
        mock_create_directory = mocker.patch(
            CREATE_DIRECTORY_MODULE_PATH, return_value=True
        )
        mocker.patch(OPEN_MODULE_PATH)
        mocker.patch.object(
            registermap.export.base.template.jinja2.Environment,
            "get_template",
            return_value=mock_template,
        )
        output_under_test.apply()

        expected_files = [
            "include/path/macro/assert.h",
            "include/path/macro/extern.h",
        ]

        assert expected_files == output_under_test.createdFiles

        mock_template.render.assert_has_calls(
            [
                mocker.call(
                    registermapName=self.expected_name, licenseText=None
                ),
                mocker.call(
                    registermapName=self.expected_name, licenseText=None
                ),
            ]
        )

        mock_create_directory.assert_has_calls(
            [
                mocker.call(output_under_test.macroDirectory),
            ]
        )


class TestAssertMacroTemplate:
    @pytest.fixture()
    def elements_context(self):
        self.expected_name = "someName"
        self.include_directory = "include/path"
        self.source_directory = "source/path"

        self.template_config = copy.deepcopy(MACRO_TEMPLATE_CONFIGURATION)
        self.template_config["files"] = [self.template_config["files"][0]]

    def test_assert_macro_output(self, elements_context):
        paths = MacroTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.sourceDirectory = self.source_directory

        output_under_test = MacroTemplates(paths, self.expected_name)
        output_under_test.configuration = self.template_config

        actual_lines = do_template_test(
            output_under_test, OPEN_MODULE_PATH, CREATE_DIRECTORY_MODULE_PATH
        )

        expected_lines = [
            "/*\n",
            " *\n",
            " *\n",
            " */\n",
            "\n",
            "#ifndef SOMENAME_ASSERT_H\n",
            "#define SOMENAME_ASSERT_H\n",
            "\n",
            "\n",
            "#ifndef DISABLE_RUNTIME_ASSERT\n",
            "\n",
            "#include <assert.h>\n",
            "#define RUNTIME_ASSERT(expression) \\\n",
            "  assert(expression)\n",
            "\n",
            "#else\n",
            "\n",
            "#define RUNTIME_ASSERT()\n",
            "\n",
            "#endif\n",
            "\n",
            "\n",
            "#ifndef DISABLE_COMPILETIME_ASSERT\n",
            "\n",
            "#define COMPILETIME_ASSERT(expression) \\\n",
            "#if !(expression) \\\n",
            '#error "ASSERTION FAILED: " #expression \\\n',
            "#endif\n",
            "\n",
            "#else\n",
            "\n",
            "#define COMPILETIME_ASSERT(expression)\n",
            "\n",
            "#endif\n",
            "\n",
            "\n",
            "#endif\n",
        ]

        assert expected_lines == actual_lines


class TestExternMacroTemplate:
    @pytest.fixture()
    def elements_context(self):
        self.expected_name = "someName"
        self.include_directory = "include/path"
        self.source_directory = "source/path"

        self.template_config = copy.deepcopy(MACRO_TEMPLATE_CONFIGURATION)
        self.template_config["files"] = [self.template_config["files"][1]]

    def test_extern_macro_output(self, elements_context):
        paths = MacroTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.sourceDirectory = self.source_directory

        output_under_test = MacroTemplates(paths, self.expected_name)
        output_under_test.configuration = self.template_config

        actual_lines = do_template_test(
            output_under_test, OPEN_MODULE_PATH, CREATE_DIRECTORY_MODULE_PATH
        )

        expected_lines = [
            "/*\n",
            " *\n",
            " *\n",
            " */\n",
            "\n",
            "#ifndef SOMENAME_EXTERN_H\n",
            "#define SOMENAME_EXTERN_H\n",
            "\n",
            "#ifdef __cplusplus\n",
            "\n",
            '#define SOMENAME_OPEN_EXTERN_C extern "C" {\n',
            "#define SOMENAME_CLOSE_EXTERN_C }\n",
            "\n",
            "#else\n",
            "\n",
            "/* Empty macro to disable extern C */\n",
            "#define SOMENAME_OPEN_EXTERN_C\n",
            "#define SOMENAME_CLOSE_EXTERN_C\n",
            "\n",
            "#endif\n",
            "\n",
            "#endif\n",
        ]

        assert expected_lines == actual_lines
