#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

import os

import jinja2
import pytest

import registermap.export.base.template
from registermap.export.c.elements import Memory
from registermap.export.c.output.memory import MemoryTemplates
from tests.ci.unit.export.common_cpp_c.output import do_template_test

CREATE_DIRECTORY_MODULE_PATH = (
    "registermap.export.c.output.memory.MemoryTemplates.createDirectory"
)
OPEN_MODULE_PATH = "registermap.export.commonCppC.output.memory.open"


class TestCMemoryTemplates:
    @pytest.fixture()
    def elements_context(self, mocker):
        self.expected_name = "someName"
        self.include_directory = "include/this/path"
        self.source_directory = "source/path"

        self.mock_memory = mocker.create_autospec(Memory)
        self.mock_memory.size = 345
        self.mock_memory.sizeType = "uint_least8_t"

    def test_defaults(self, elements_context):
        expected_memory_directory = os.path.join(
            self.include_directory, "memory"
        )
        expected_template_directory = os.path.join("templates", "idiomatic")

        paths = MemoryTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.includePrefix = "this/path"
        paths.sourceDirectory = self.source_directory

        output_under_test = MemoryTemplates(
            paths, self.expected_name, self.mock_memory
        )

        assert self.expected_name == output_under_test.registerMapName
        assert (
            expected_template_directory == output_under_test.templateDirectory
        )
        assert expected_memory_directory == output_under_test.memoryDirectory
        assert self.mock_memory == output_under_test.encapsulatedMemory

    def test_initial_created_files(self, elements_context, mocker):
        paths = MemoryTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.includePrefix = "this/path"
        paths.sourceDirectory = self.source_directory

        output_under_test = MemoryTemplates(
            paths, self.expected_name, self.mock_memory
        )
        mock_template = mocker.create_autospec(jinja2.Template)
        mock_create_directory = mocker.patch(
            CREATE_DIRECTORY_MODULE_PATH, return_value=True
        )
        mocker.patch(OPEN_MODULE_PATH)
        mocker.patch.object(
            registermap.export.base.template.jinja2.Environment,
            "get_template",
            return_value=mock_template,
        )
        output_under_test.apply()

        expected_files = ["include/this/path/memory/memory.h"]

        assert expected_files == output_under_test.createdFiles

        mock_template.render.assert_called_once()

        mock_create_directory.assert_has_calls(
            [
                mocker.call(output_under_test.memoryDirectory),
            ]
        )

    def test_memory_output(self, elements_context):
        paths = MemoryTemplates.Paths()
        paths.includeDirectory = self.include_directory
        paths.includePrefix = "this/prefix"
        paths.sourceDirectory = self.source_directory

        output_under_test = MemoryTemplates(
            paths, self.expected_name, self.mock_memory
        )
        actual_lines = do_template_test(
            output_under_test, OPEN_MODULE_PATH, CREATE_DIRECTORY_MODULE_PATH
        )

        expected_lines = [
            "/*\n",
            " *\n",
            " * someName\n",
            " *\n",
            " *\n",
            " */\n",
            "\n",
            "#ifndef SOMENAME_MEMORY_H\n",
            "#define SOMENAME_MEMORY_H\n",
            "\n",
            "#include <stdint.h>\n",
            "\n",
            '#include "this/prefix/macro/extern.h"\n',
            "\n",
            "\n",
            "SOMENAME_OPEN_EXTERN_C\n",
            "\n",
            "struct someName_MemorySpace_t\n",
            "{\n",
            "#ifdef OFF_TARGET_MEMORY\n",
            "\n",
            "  uint_least32_t const allocated_memory_span;\n",
            "\n",
            "  uint_least8_t volatile base[ 345 ];\n",
            "\n",
            "#else\n",
            "\n",
            "  uint_least8_t volatile* const base;\n",
            "\n",
            "#endif\n",
            "};\n",
            "\n",
            "SOMENAME_CLOSE_EXTERN_C\n",
            "\n",
            "#endif\n",
        ]

        assert expected_lines == actual_lines
