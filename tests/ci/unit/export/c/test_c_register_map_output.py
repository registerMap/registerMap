#
# Copyright 2018 Russell Smiley
#
# This file is part of registermap.
#
# registermap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registermap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap import RegisterMap
from registermap.export.c.options import COptions
from registermap.export.c.registerMap import Output as COutput


class TestCRegisterMapHeaderSource:
    @pytest.fixture()
    def elements_context(self):
        self.output_dir = "some/path"

        self.__setup_register_map()

        self.mock_options = COptions()
        self.mock_options.includePrefix = "this/prefix"
        self.mock_options.output = self.output_dir
        self.mock_options.packAlignment = 10

    def __setup_register_map(self):
        self.registermap = RegisterMap()

        self.registermap.memory.baseAddress = 0x20F0

        m1 = self.registermap.addModule("m1")
        r1 = m1.addRegister("r1")
        r1.addField("f1", (0, 11))

        m2 = self.registermap.addModule("m2")
        r2 = m2.addRegister("r2")
        r2.addField("f2", (0, 4))

        m3 = self.registermap.addModule("m3")
        r3 = m3.addRegister("r3")
        r3.addField("f3", (4, 6))

    def testExecution(self, elements_context, mocker):
        mocker.patch(
            "registermap.export.c.registerMap.OutputBase._OutputBase__validateOutputDirectory"
        )
        mock_macro_template = mocker.patch(
            "registermap.export.c.registerMap.MacroTemplates"
        )
        mock_memory_template = mocker.patch(
            "registermap.export.c.registerMap.MemoryTemplates"
        )
        mock_module_template = mocker.patch(
            "registermap.export.c.registerMap.ModuleTemplates"
        )
        mock_register_map_template = mocker.patch(
            "registermap.export.c.registerMap.RegisterMapTemplates"
        )
        mocker.patch(
            "registermap.export.c.registerMap.Output._Output__createDirectories"
        )
        output_under_test = COutput(self.mock_options)

        # Mock created directory paths
        output_under_test.includePath = "some/include"
        output_under_test.source_directory = "some/source"

        output_under_test.generate(self.registermap, "myRegisterMap")

        mock_macro_template_instance = mock_macro_template.return_value
        mock_macro_template_instance.apply.assert_called_once()

        mock_memory_template_instance = mock_memory_template.return_value
        mock_memory_template_instance.apply.assert_called_once()

        mock_module_template_instance = mock_module_template.return_value
        mock_module_template_instance.apply.assert_called_once()

        mock_registermap_template_instance = (
            mock_register_map_template.return_value
        )
        mock_registermap_template_instance.apply.assert_called_once()
