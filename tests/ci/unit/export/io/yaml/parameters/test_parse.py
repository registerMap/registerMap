#
# Copyright 2017 Russell Smiley
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.export.io.yaml.parameters.parse import (
    ParseError,
    booleanParameter,
    complexParameter,
    integerParameter,
    stringParameter,
)


class TestBooleanParameter:
    def test_good_data(self):
        def record_result(value):
            nonlocal actual_value
            actual_value = value

        key_name = "name"
        expected_value = True
        yaml_data = {key_name: expected_value}

        actual_value = None
        good_result = booleanParameter(yaml_data, key_name, record_result)

        assert good_result
        assert actual_value == expected_value


class TestStringParameter:
    def test_good_data(self):
        def record_result(value):
            nonlocal actual_value
            actual_value = value

        key_name = "name"
        expected_value = "value"

        yaml_data = {key_name: expected_value}

        actual_value = None
        good_result = stringParameter(yaml_data, key_name, record_result)

        assert good_result
        assert actual_value == expected_value

    def test_bad_key(self):
        def record_result(value):
            nonlocal actual_value
            actual_value = value

        key_name = "name"
        expected_value = "value"

        yaml_data = {key_name: expected_value}

        actual_value = None
        with pytest.raises(ParseError):
            stringParameter(yaml_data, "parameter", record_result)


class TestIntegerParameter:
    def test_good_data(self):
        def record_result(value):
            nonlocal actual_value
            actual_value = value

        key_name = "offset"
        expected_value = 10

        yaml_data = {key_name: expected_value}

        actual_value = None
        good_result = integerParameter(yaml_data, key_name, record_result)

        assert good_result
        assert actual_value == expected_value

    def test_bad_value(self):
        def record_result(value):
            nonlocal actual_value
            actual_value = value

        key_name = "offset"
        expected_value = "value"

        yaml_data = {key_name: expected_value}

        actual_value = None
        good_result = integerParameter(yaml_data, key_name, record_result)

        assert not good_result
        assert actual_value is None

    def test_none_value_valid(self):
        def record_result(value):
            nonlocal actual_value
            actual_value = value

        key_name = "offset"
        expected_value = None

        yaml_data = {key_name: expected_value}

        actual_value = None
        good_result = integerParameter(
            yaml_data, key_name, record_result, noneValid=True
        )

        assert good_result
        assert actual_value is None

    def test_none_value_invalid(self):
        def record_result(value):
            nonlocal actual_value
            actual_value = value

        key_name = "offset"
        expected_value = None

        yaml_data = {key_name: expected_value}

        actual_value = None
        good_result = integerParameter(yaml_data, key_name, record_result)

        assert not good_result
        assert actual_value is None

    def test_use_name_active(self):
        def record_result(name, value):
            nonlocal actual_name, actual_value
            actual_name = name
            actual_value = value

        key_name = "offset"
        expected_value = 3

        yaml_data = {key_name: expected_value}

        actual_name = None
        actual_value = None
        good_result = integerParameter(
            yaml_data, key_name, record_result, useName=True
        )

        assert good_result
        assert actual_name == key_name
        assert actual_value == expected_value


class TestComplexParameter:
    def test_good_data(self):
        def do_parse_action(this_data):
            nonlocal actual_data

            this_good_result = True
            actual_data = []
            for element in this_data:
                try:
                    value = element["name"]
                    actual_data.append(value)
                except KeyError:
                    this_good_result = False

            return this_good_result

        key_name = "fields"

        yaml_data = {
            key_name: [{"name": "one"}, {"name": "two"}, {"name": "three"}]
        }

        actual_data = None
        good_result = complexParameter(yaml_data, key_name, do_parse_action)

        assert good_result
        assert actual_data == ["one", "two", "three"]

    def test_bad_value(self):
        def do_parse_action(thisData):
            nonlocal actual_data

            this_good_result = False

            return this_good_result

        key_name = "fields"

        yaml_data = {
            "badName": [{"name": "one"}, {"name": "two"}, {"name": "three"}]
        }

        actual_data = None
        with pytest.raises(ParseError):
            complexParameter(yaml_data, key_name, do_parse_action)
