#
# Copyright 2017 Russell Smiley
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import pathlib
import tempfile

from registermap.export.io.yaml.parameters.encode import parameter
from registermap.export.io.yaml.stream import load, save


class TestEncodeParameter:
    def test_save_load(self):
        name_stub = self.test_save_load.__name__
        yaml_data = parameter("someParameter", 10)

        _test_save_load(self, name_stub, yaml_data)


def _test_save_load(instance, name_stub, yaml_data):
    with tempfile.TemporaryDirectory() as d:
        this_dir = pathlib.Path(d)
        file_path_name = os.path.join(this_dir, (name_stub + ".yml"))

        save(file_path_name, yaml_data)

        recovered_data = load(file_path_name)

        assert yaml_data == recovered_data
