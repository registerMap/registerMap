#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap import RegisterMap
from registermap.export.base.register import RegisterBase

from .mocks import MockField, MockRegister


class TestExportRegisterBase:
    @pytest.fixture()
    def elements_context(self):
        self.register_map = RegisterMap()
        self.module1 = self.register_map.addModule("m1")

        self.register1 = self.module1.addRegister("r1")

        self.export_under_test = MockRegister(self.register1, MockField)

    def test_name(self, elements_context):
        assert self.register1["name"] == self.export_under_test.name

    def test_empty_register(self, elements_context):
        assert 0 == len(self.export_under_test.fields)

    def test_address_property(self, elements_context):
        assert (
            self.export_under_test.expectedAddress
            == self.export_under_test.address
        )

    def test_offset_property(self, elements_context):
        assert (
            self.export_under_test.expectedOffset
            == self.export_under_test.offset
        )

    def test_fields(self, elements_context):
        self.register1.addField("f1", (0, 2))
        self.register1.addField("f2", (3, 4))
        self.register1.addField("f3", (5, 7))

        for expectedName, expectedSize, thisField in zip(
            ["f1", "f2", "f3"], [3, 2, 3], self.export_under_test.fields
        ):
            assert expectedName == thisField.name
            assert expectedSize == thisField.size


class TestRegisterBasePrecedingGapProperty:
    @pytest.fixture()
    def elements_context(self):
        self.register_map = RegisterMap()
        self.module1 = self.register_map.addModule("m1")

        self.register1 = self.module1.addRegister("r1")

        assert 0 == self.register1.startAddress

    def test_type(self, elements_context):
        r2 = self.module1.addRegister("r2")

        assert 1 == r2.startAddress

        export_under_test = RegisterBase(r2, MockField)

        assert isinstance(export_under_test.precedingGapBytes, int)

    def test_zero_gap(self, elements_context):
        r2 = self.module1.addRegister("r2")

        assert 1 == r2.startAddress

        export_under_test = RegisterBase(r2, MockField)

        assert 0 == export_under_test.precedingGapBytes

    def test_non_zero_gap(self, elements_context):
        r2 = self.module1.addRegister("r2")
        r2["constraints"]["fixedAddress"] = 0x10

        assert 0x10 == r2.startAddress

        export_under_test = RegisterBase(r2, MockField)

        assert (0x10 - 1) == export_under_test.precedingGapBytes

    def test_non_zero_gap16_bit_memory_unit(self, elements_context):
        self.module1.memory.memoryUnitBits = 16

        r2 = self.module1.addRegister("r2")
        r2["constraints"]["fixedAddress"] = 0x10

        assert 0x10 == r2.startAddress

        export_under_test = RegisterBase(r2, MockField)

        assert ((0x10 - 1) * 2) == export_under_test.precedingGapBytes
