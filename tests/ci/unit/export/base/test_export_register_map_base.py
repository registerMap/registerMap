#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap import RegisterMap
from registermap.export.base.memory import MemoryBase
from registermap.export.base.registerMap import RegisterMapBase

from .mocks import MockField, MockMemory, MockModule, MockRegister

MOCK_TYPE_CONFIGURATION = {
    "field": MockField,
    "memory": MockMemory,
    "module": MockModule,
    "register": MockRegister,
}


class TestRegisterMapBase:
    @pytest.fixture()
    def elements_context(self):
        self.register_map = RegisterMap()

        m1 = self.register_map.addModule("m1")
        m1 = self.register_map.addModule("m2")

    def test_memory_property(self, elements_context):
        exporter_under_test = RegisterMapBase(
            "thisRegisterMap", self.register_map, MOCK_TYPE_CONFIGURATION
        )

        assert isinstance(exporter_under_test.memory, MemoryBase)
        assert (
            self.register_map.spanMemoryUnits == exporter_under_test.memory.size
        )

    def test_module_indirection(self, elements_context):
        exporter_under_test = RegisterMapBase(
            "thisRegisterMap", self.register_map, MOCK_TYPE_CONFIGURATION
        )

        for expected_name, thisModule in zip(
            ["m1", "m2"], exporter_under_test.modules
        ):
            assert expected_name == thisModule.name

    def test_span_memory_units_property(self, elements_context):
        exporter_under_test = RegisterMapBase(
            "thisRegisterMap", self.register_map, MOCK_TYPE_CONFIGURATION
        )

        assert (
            self.register_map.spanMemoryUnits
            == exporter_under_test.spanMemoryUnits
        )
