#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import os

import pytest

from registermap.export.base.template import TemplateBase


class TestExportTemplateBase:
    @pytest.fixture()
    def elements_context(self):
        self.include_directory = "include/path"
        self.source_directory = "source/path"

    def test_defaults(self, elements_context):
        here = os.path.abspath(os.path.dirname(__file__))
        expected_template_dir = "templates"

        paths = TemplateBase.Paths()
        paths.includeDirectory = self.include_directory
        paths.sourceDirectory = self.source_directory
        paths.templatePackagePath = "registermap.export.c.output"

        output_under_test = TemplateBase(paths)

        assert expected_template_dir == output_under_test.templateDirectory
        assert (
            self.include_directory == output_under_test.paths.includeDirectory
        )
        assert self.source_directory == output_under_test.paths.sourceDirectory
        assert list() == output_under_test.createdDirectories
        assert list() == output_under_test.createdFiles

    def test_template_license_text_lines(self, elements_context):
        lines = [
            "this line",
            "another line",
        ]

        paths = TemplateBase.Paths()
        paths.includeDirectory = self.include_directory
        paths.sourceDirectory = self.source_directory
        paths.templatePackagePath = "registermap.export.c.output"

        output_under_test = TemplateBase(paths, licenseTextLines=lines)

        assert lines == output_under_test.licenseTextLines

    def test_template_subdir(self, elements_context, mocker):
        subdir = "thisSubdir"

        expected_value = os.path.join("templates", subdir)

        paths = TemplateBase.Paths()
        paths.includeDirectory = self.include_directory
        paths.sourceDirectory = self.source_directory
        paths.templatePackagePath = "registermap.export.c.output"

        pl = mocker.patch(
            "registermap.export.base.template.jinja2.PackageLoader"
        )

        output_under_test = TemplateBase(paths, subdir=subdir)

        actual_value = output_under_test.templateDirectory

        pl.assert_called_once_with(
            mocker.ANY, os.path.join("templates", subdir)
        )
        assert expected_value == actual_value

    def test_create_directory(self, elements_context, mocker):
        paths = TemplateBase.Paths()
        paths.includeDirectory = self.include_directory
        paths.sourceDirectory = self.source_directory
        paths.templatePackagePath = "registermap.export.c.output"

        output_under_test = TemplateBase(paths)

        expected_value = "some/path"

        mock_makedirs = mocker.patch(
            "registermap.export.base.template.os.makedirs"
        )
        output_under_test.createDirectory(expected_value)

        mock_makedirs.assert_called_once_with(expected_value, exist_ok=True)

        assert 1 == len(output_under_test.createdDirectories)
        assert expected_value in output_under_test.createdDirectories
