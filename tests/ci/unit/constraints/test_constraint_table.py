#
# Copyright 2016 Russell Smiley
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import enum
import logging
import math

import pytest

from registermap.constraints.constraintTable import (
    ConstraintError,
    ConstraintTable,
)
from registermap.structure.memory.configuration import MemoryConfiguration
from tests.ci.unit.structure.elements.mock_observer import MockObserver

log = logging.getLogger(__name__)


class MockItem:
    """Mock the interfaces of RegisterMap, Module, Register, BitField that are used by constraints."""

    def __init__(
        self, initial_address, size_value, memory=MemoryConfiguration()
    ):
        self.__memory = memory

    @property
    def memory(self) -> MemoryConfiguration:
        return self.__memory


@enum.unique
class NotifyType(enum.Enum):
    ADDRESS = 1
    SIZE = 2


class ElementsContext:
    constraints: ConstraintTable
    memory: MemoryConfiguration
    observer: MockObserver

    def __init__(
        self,
        this_memory: MemoryConfiguration,
        notify_type: NotifyType = NotifyType.ADDRESS,
    ) -> None:
        self.memory = this_memory

        self.constraints = ConstraintTable(self.memory)
        self.observer = MockObserver()
        if notify_type == NotifyType.ADDRESS:
            self.constraints.addressChangeNotifier.addObserver(self.observer)
        elif notify_type == NotifyType.SIZE:
            self.constraints.sizeChangeNotifier.addObserver(self.observer)
        else:
            raise ValueError("Invalid notify_type")


@pytest.fixture()
def this_memory() -> MemoryConfiguration:
    return MemoryConfiguration()


@pytest.fixture()
def address_constraint_context(this_memory) -> ElementsContext:
    return ElementsContext(this_memory, NotifyType.ADDRESS)


@pytest.fixture()
def size_constraint_context(this_memory) -> ElementsContext:
    return ElementsContext(this_memory, NotifyType.SIZE)


class TestConstraintTable:
    def test_query_empty_constraint_table_raises(
        self, address_constraint_context
    ):
        for thisName in ConstraintTable.VALID_CONSTRAINTS.keys():
            with pytest.raises(
                ConstraintError, match="^Constraint not applied"
            ):
                address_constraint_context.constraints[thisName]

    def test_invalid_constraint_name_raises(self, address_constraint_context):
        with pytest.raises(ConstraintError, match="^Not a valid constraint"):
            address_constraint_context.constraints["badName"]

    def test_add_valid_constraint(
        self, address_constraint_context: ElementsContext
    ):
        expected_value = 0x10
        address_constraint_context.constraints["fixedAddress"] = expected_value

        assert (
            address_constraint_context.constraints["fixedAddress"]
            == expected_value
        )
        assert address_constraint_context.observer.update_count == 1

    def test_add_invalid_constraint_raises(
        self, address_constraint_context: ElementsContext
    ):
        with pytest.raises(ConstraintError, match="^Not a valid constraint"):
            address_constraint_context.constraints["badName"] = 0x10

    def test_delete_constraint(
        self, address_constraint_context: ElementsContext
    ):
        constraint_name = "fixedAddress"
        expected_value = 0x10
        address_constraint_context.constraints[constraint_name] = expected_value

        assert (
            address_constraint_context.constraints[constraint_name]
            == expected_value
        )
        assert address_constraint_context.observer.update_count == 1

        del address_constraint_context.constraints[constraint_name]
        assert address_constraint_context.observer.update_count == 2

        with pytest.raises(ConstraintError, match="^Constraint not applied"):
            address_constraint_context.constraints[constraint_name]

    def test_two_constraints(self, address_constraint_context: ElementsContext):
        initial_address = 0x10
        address_constraint_context.constraints["alignmentMemoryUnits"] = 4
        address_constraint_context.constraints["fixedAddress"] = initial_address
        assert address_constraint_context.observer.update_count == 2

        # The fixed address constraint takes precedence
        actual_address = (
            address_constraint_context.constraints.applyAddressConstraints(
                initial_address
            )
        )
        assert actual_address == initial_address

    def test_two_constraints_different_order(
        self, address_constraint_context: ElementsContext
    ):
        initial_address = 0x10
        address_constraint_context.constraints["fixedAddress"] = initial_address
        address_constraint_context.constraints["alignmentMemoryUnits"] = 2
        assert address_constraint_context.observer.update_count == 2

        # The fixed address constraint takes precedence
        actualAddress = (
            address_constraint_context.constraints.applyAddressConstraints(
                initial_address
            )
        )
        assert actualAddress == initial_address

    def test_add_bad_fixed_address_against_alignment_raises(
        self, address_constraint_context: ElementsContext
    ):
        expected_address = 0x1
        address_constraint_context.constraints["alignmentMemoryUnits"] = 4
        with pytest.raises(
            ConstraintError, match="^Address constraints conflict"
        ):
            address_constraint_context.constraints[
                "fixedAddress"
            ] = expected_address

    def test_add_alignment_against_bad_fixed_address_raises(
        self, address_constraint_context: ElementsContext
    ):
        expected_address = 0x1
        address_constraint_context.constraints[
            "fixedAddress"
        ] = expected_address
        with pytest.raises(
            ConstraintError, match="^Address constraints conflict"
        ):
            address_constraint_context.constraints["alignmentMemoryUnits"] = 4

    def test_fixed_address_on_page_register_raises(
        self, address_constraint_context: ElementsContext
    ):
        address_constraint_context.memory.pageSize = 128
        number_memory_units = math.ceil(
            float(address_constraint_context.memory.addressBits)
            / address_constraint_context.memory.memoryUnitBits
        )
        page_addresses = [
            (address_constraint_context.memory.pageSize - x + 0x800)
            for x in range(1, (number_memory_units + 1))
        ]

        for address in page_addresses:
            with pytest.raises(
                ConstraintError,
                match="^Cannot constrain address to page register",
            ):
                address_constraint_context.constraints["fixedAddress"] = address

    def test_delete_invalid_constraint_raises(
        self, address_constraint_context: ElementsContext
    ):
        constraint_name = "badName"
        with pytest.raises(ConstraintError, match="^Not a valid constraint"):
            del address_constraint_context.constraints[constraint_name]

    def test_delete_valid_constraint_no_applied_raises(
        self, address_constraint_context: ElementsContext
    ):
        constraint_name = "fixedAddress"
        with pytest.raises(ConstraintError, match="^Constraint not applied"):
            del address_constraint_context.constraints[constraint_name]

    def test_is_empty(self, address_constraint_context: ElementsContext):
        assert address_constraint_context.constraints.isEmpty

        address_constraint_context.constraints["fixedAddress"] = 0x10

        assert not address_constraint_context.constraints.isEmpty


class TestApplyConstraints:
    def test_apply_address_constraints_no_constraints(
        self, address_constraint_context: ElementsContext
    ):
        # with no constraints, the initial address is unchanged.
        expected_address = 0x10

        actual_address = (
            address_constraint_context.constraints.applyAddressConstraints(
                expected_address
            )
        )

        assert actual_address == expected_address

    def test_apply_size_constraint_no_constraints(
        self, address_constraint_context: ElementsContext
    ):
        # with no constraints, the initial address is unchanged.
        expected_size = 0x10

        actual_size = (
            address_constraint_context.constraints.applySizeConstraints(
                expected_size
            )
        )

        assert actual_size == expected_size


class TestApplyFixedAddress:
    def setUp(self):
        address_constraint_context.memory = MemoryConfiguration()
        address_constraint_context.constraints = ConstraintTable(
            address_constraint_context.memory
        )
        address_constraint_context.observer = MockObserver()

        address_constraint_context.constraints.addressChangeNotifier.addObserver(
            address_constraint_context.observer
        )

    def test_fixed_address_changes(
        self, address_constraint_context: ElementsContext
    ):
        expected_address = 0x10

        address_constraint_context.constraints[
            "fixedAddress"
        ] = expected_address

        assert (
            address_constraint_context.constraints["fixedAddress"]
            == expected_address
        )
        assert address_constraint_context.observer.update_count, 1

    def test_negative_raises(self, address_constraint_context: ElementsContext):
        with pytest.raises(
            ConstraintError,
            match="^Fixed address constraint must be a positive integer",
        ):
            address_constraint_context.constraints["fixedAddress"] = -1

    def test_current_address_exceeds_fixed_address_raises(
        self, address_constraint_context: ElementsContext
    ):
        current_address = 0x11
        fixed_address = 0x10

        address_constraint_context.constraints["fixedAddress"] = fixed_address

        with pytest.raises(ConstraintError, match="^Fixed address exceeded"):
            address_constraint_context.constraints.applyAddressConstraints(
                current_address
            )

    def test_current_address_equals_fixed_address(
        self, address_constraint_context: ElementsContext
    ):
        expected_address = 0x11

        address_constraint_context.constraints[
            "fixedAddress"
        ] = expected_address

        actual_address = (
            address_constraint_context.constraints.applyAddressConstraints(
                expected_address
            )
        )

        assert actual_address == expected_address

    def test_current_address_is_none(
        self, address_constraint_context: ElementsContext
    ):
        expected_address = 0x11

        address_constraint_context.constraints[
            "fixedAddress"
        ] = expected_address

        actual_address = (
            address_constraint_context.constraints.applyAddressConstraints(None)
        )

        assert actual_address == expected_address


class TestApplyAddressAlignment:
    def test_address_alignment_change(
        self, address_constraint_context: ElementsContext
    ):
        alignment_value = 4
        initial_address = 0x6
        expected_address = 0x8
        assert initial_address < expected_address
        assert (initial_address % alignment_value) != 0

        address_constraint_context.constraints[
            "alignmentMemoryUnits"
        ] = alignment_value

        assert address_constraint_context.observer.update_count, 1

        actual_address = (
            address_constraint_context.constraints.applyAddressConstraints(
                initial_address
            )
        )

        assert actual_address == expected_address

    def test_address_alignment_no_change(
        self, address_constraint_context: ElementsContext
    ):
        alignment_value = 3
        initial_address = 0x6
        expected_address = initial_address
        assert (initial_address % alignment_value) == 0

        address_constraint_context.constraints[
            "alignmentMemoryUnits"
        ] = alignment_value

        actual_address = (
            address_constraint_context.constraints.applyAddressConstraints(
                initial_address
            )
        )

        assert actual_address == expected_address

    def test_zero_raises(self, address_constraint_context: ElementsContext):
        with pytest.raises(
            ConstraintError,
            match="^Alignment must be a positive non-zero integer",
        ):
            address_constraint_context.constraints["alignmentMemoryUnits"] = 0

    def test_negative_raises(self, address_constraint_context: ElementsContext):
        with pytest.raises(
            ConstraintError,
            match="^Alignment must be a positive non-zero integer",
        ):
            address_constraint_context.constraints["alignmentMemoryUnits"] = -1


class TestApplySize:
    def test_under_size_ok(self, size_constraint_context: ElementsContext):
        fixed_size = 6
        size_constraint_context.constraints["fixedSizeMemoryUnits"] = fixed_size
        assert size_constraint_context.observer.update_count == 1

    def test_zero_no_raise(self, address_constraint_context: ElementsContext):
        address_constraint_context.constraints["fixedSizeMemoryUnits"] = 0

    def test_negative_raises(self, address_constraint_context: ElementsContext):
        with pytest.raises(
            ConstraintError, match="^Fixed size must be a positive integer"
        ):
            address_constraint_context.constraints["fixedSizeMemoryUnits"] = -1

    def test_over_size_raises(
        self, address_constraint_context: ElementsContext
    ):
        fixed_size = 6
        current_size = 8

        assert current_size > fixed_size

        address_constraint_context.constraints[
            "fixedSizeMemoryUnits"
        ] = fixed_size

        with pytest.raises(ConstraintError, match="^Fixed size exceeded"):
            address_constraint_context.constraints.applySizeConstraints(
                current_size
            )


class TestConstraintTableLen:
    def test_empty_equals_zero(
        self, address_constraint_context: ElementsContext
    ):
        assert len(address_constraint_context.constraints) == 0

    def test_add_one_constraint_len_equals_one(
        self, address_constraint_context: ElementsContext
    ):
        assert len(address_constraint_context.constraints) == 0

        address_constraint_context.constraints["fixedAddress"] = 0x10
        assert len(address_constraint_context.constraints) == 1


class TestLoadSave:
    def test_encode_decode(self, address_constraint_context: ElementsContext):
        address_constraint_context.constraints["fixedAddress"] = 0x10
        address_constraint_context.constraints["fixedSizeMemoryUnits"] = 0x7
        address_constraint_context.constraints["alignmentMemoryUnits"] = 2

        encoded_yaml_data = address_constraint_context.constraints.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_data = ConstraintTable.from_yamlData(
            encoded_yaml_data, address_constraint_context.memory
        )

        assert (
            decoded_data["fixedAddress"]
            == address_constraint_context.constraints["fixedAddress"]
        )
        assert (
            decoded_data["fixedSizeMemoryUnits"]
            == address_constraint_context.constraints["fixedSizeMemoryUnits"]
        )
        assert (
            decoded_data["alignmentMemoryUnits"]
            == address_constraint_context.constraints["alignmentMemoryUnits"]
        )

    def test_decode_empty(self, address_constraint_context: ElementsContext):
        empty_constraints_yaml_data = {"constraints": {}}
        table = ConstraintTable.from_yamlData(
            empty_constraints_yaml_data, address_constraint_context.memory
        )

        assert table.isEmpty


class TestLimitedConstraintSet:
    def setUp(self):
        address_constraint_context.memory = MemoryConfiguration()

    def test_default_valid_constraints(self, this_memory: MemoryConfiguration):
        this_constraints = ConstraintTable(this_memory)

        assert (
            set(this_constraints.VALID_CONSTRAINTS.keys())
            == this_constraints.currentlyValidConstraints
        )

    def test_limited_constraint_applies_constraint(
        self, this_memory: MemoryConfiguration
    ):
        """Applying a constraint in the limited constraint set applies the constraint."""

        limited_constraints = {"fixedAddress"}

        this_constraints = ConstraintTable(
            this_memory, validConstraints=limited_constraints
        )

        expected_address = 0x11

        this_constraints["fixedAddress"] = expected_address

        actual_address = this_constraints.applyAddressConstraints(
            expected_address
        )

        assert actual_address == expected_address

    def test_excluded_constraint_raises(self, this_memory: MemoryConfiguration):
        """Applying a constraint that has been excluded raises an exception"""

        excluded_constraint = "fixedSizeMemoryUnits"
        limited_constraints = {"fixedAddress"}

        assert excluded_constraint not in limited_constraints

        this_constraints = ConstraintTable(
            this_memory, validConstraints=limited_constraints
        )

        with pytest.raises(
            ConstraintError,
            match="^Constraint has been excluded from this ConstraintTable",
        ):
            this_constraints[excluded_constraint] = 10
