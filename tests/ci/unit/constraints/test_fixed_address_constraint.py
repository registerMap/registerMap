#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import logging
import math

import pytest

from registermap.constraints.constraints import (
    AbstractConstraint,
    ConstraintError,
    FixedAddress,
    ParseError,
)
from registermap.structure.memory.configuration import MemoryConfiguration

log = logging.getLogger(__name__)


class ElementContext:
    def __init__(self):
        self.memory = MemoryConfiguration()
        self.constraint = FixedAddress(self.memory)


@pytest.fixture()
def fixed_address_constraint() -> ElementContext:
    return ElementContext()


@pytest.fixture()
def verified_fixed_address_constraint() -> ElementContext:
    # Verify that the memory defaults are what are assumed in these tests.
    v = ElementContext()
    assert v.memory.addressBits == 32
    assert v.memory.memoryUnitBits == 8

    return v


class TestFixedAddress:
    def test_name(self, fixed_address_constraint):
        assert fixed_address_constraint.constraint.name == FixedAddress.name

    def test_type(self, fixed_address_constraint):
        assert (
            fixed_address_constraint.constraint.type
            == AbstractConstraint.constraintTypes["address"]
        )


class TestFixedAddressCalculation:
    def test_address_over_raises(self, fixed_address_constraint):
        expected_address = 0x12

        fixed_address_constraint.constraint.value = expected_address

        with pytest.raises(ConstraintError, match="^Fixed address exceeded"):
            fixed_address_constraint.constraint.calculate(0x14)

    def test_constraint_calculation(self, fixed_address_constraint):
        expected_address = 0x12

        fixed_address_constraint.constraint.value = expected_address

        actactual_addressalAddress = (
            fixed_address_constraint.constraint.calculate(0x10)
        )

        assert actactual_addressalAddress == expected_address

    def test_fixed_address_on_page_register_raises(
        self, fixed_address_constraint
    ):
        fixed_address_constraint.memory.pageSize = 128

        number_memory_units = math.ceil(
            float(fixed_address_constraint.memory.addressBits)
            / fixed_address_constraint.memory.memoryUnitBits
        )
        page_addresses = [
            (fixed_address_constraint.memory.pageSize - x + 0x800)
            for x in range(1, (number_memory_units + 1))
        ]

        for address in page_addresses:
            with pytest.raises(
                ConstraintError,
                match="^Cannot constrain address to page register",
            ):
                fixed_address_constraint.constraint.value = address

    def test_non_int_address_asserts(self, fixed_address_constraint):
        with pytest.raises(
            ConstraintError, match="^Fixed address must be a positive integer"
        ):
            fixed_address_constraint.constraint.calculate("0x14")

    def test_none_address(self, fixed_address_constraint):
        expected_address = 0x12

        fixed_address_constraint.constraint.value = expected_address

        actual_address = fixed_address_constraint.constraint.calculate(None)

        assert actual_address == expected_address


class TestFixedAddressValidation:
    def test_no_page_register_passes(self, verified_fixed_address_constraint):
        # No page register means any address should pass
        verified_fixed_address_constraint.constraint.value = 0x12
        verified_fixed_address_constraint.constraint.validate(0x14)

    def test_none_address_passes(self, verified_fixed_address_constraint):
        verified_fixed_address_constraint.constraint.validate(None)

    def test_fixed_address_on_page_register_raises(
        self, verified_fixed_address_constraint
    ):
        verified_fixed_address_constraint.memory.pageSize = 0x13

        with pytest.raises(
            ConstraintError, match="^Cannot constrain address to page register"
        ):
            verified_fixed_address_constraint.constraint.value = 0x12

    def testFixedAddressOnPageRegisterRaisesWhenPageSizeIntroduced(
        self, verified_fixed_address_constraint
    ):
        verified_fixed_address_constraint.constraint.value = 0x12

        with pytest.raises(
            ConstraintError, match="^Cannot constrain address to page register"
        ):
            # Changing the memory page size is expected to notify the constraint which will now raise because it is no
            # longer valid.
            verified_fixed_address_constraint.memory.pageSize = 0x13

    def testFixedAddressNotOnPageRegisterPasses(
        self, verified_fixed_address_constraint
    ):
        assert verified_fixed_address_constraint.memory.addressBits == 32

        verified_fixed_address_constraint.memory.pageSize = 0x13
        verified_fixed_address_constraint.constraint.value = (
            verified_fixed_address_constraint.memory.pageSize - 4 - 1
        )

        assert not verified_fixed_address_constraint.memory.isPageRegister(
            verified_fixed_address_constraint.constraint.value
        )

        verified_fixed_address_constraint.constraint.validate(0x12)


class TestFixedAddressMemoryUnitsValueProperty:
    def test_get_property(self, fixed_address_constraint):
        expected_value = 14
        fixed_address_constraint.constraint.value = expected_value

        actual_value = fixed_address_constraint.constraint.value

        assert actual_value == expected_value

    def test_zero_value_no_raise(self, fixed_address_constraint):
        expected_value = 0
        fixed_address_constraint.constraint.value = expected_value

        actual_value = fixed_address_constraint.constraint.value

        assert actual_value == expected_value

    def test_none_value_asserts(self, fixed_address_constraint):
        with pytest.raises(AssertionError):
            fixed_address_constraint.constraint.value = None


class TestLoadSave:
    def testEncodeDecode(self, fixed_address_constraint):
        fixed_address_constraint.constraint.value = 4

        encoded_yaml_data = fixed_address_constraint.constraint.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_data = FixedAddress.from_yamlData(
            encoded_yaml_data, fixed_address_constraint.memory
        )

        assert decoded_data.value == fixed_address_constraint.constraint.value

    def test_decode_non_alignment_constraint_raises(
        self, fixed_address_constraint
    ):
        with pytest.raises(
            ParseError,
            match="^Yaml data does not contain fixed address constraint",
        ):
            FixedAddress.from_yamlData({}, fixed_address_constraint.memory)

    def test_decode_optional_alignment_constraint_raises(
        self, fixed_address_constraint
    ):
        constraint = FixedAddress.from_yamlData(
            {}, fixed_address_constraint.memory, optional=True
        )

        assert constraint is None
