#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

import pytest

from registermap.constraints.constraints import (
    AbstractConstraint,
    ConstraintError,
    FixedAddress,
    FixedSizeMemoryUnits,
    ParseError,
)
from registermap.structure.memory.configuration import MemoryConfiguration

log = logging.getLogger(__name__)


class ElementsContext:
    def __init__(self):
        self.memory = MemoryConfiguration()
        self.constraint = FixedSizeMemoryUnits(self.memory)


@pytest.fixture()
def elements_context():
    return ElementsContext()


class TestFixedSizeMemoryUnits:
    def test_name(self, elements_context):
        assert elements_context.constraint.name == FixedSizeMemoryUnits.name

    def test_type(self, elements_context):
        assert (
            elements_context.constraint.type
            == AbstractConstraint.constraintTypes["size"]
        )


def do_none_constraint_asserts(method_under_test):
    with pytest.raises(AssertionError):
        method_under_test(14)


def do_non_int_size_raises(method_under_test):
    with pytest.raises(AssertionError):
        method_under_test("12")


def do_negative_size_raises(
    elements_context: ElementsContext, method_under_test
):
    elements_context.constraint.sizeConstraint = 12

    with pytest.raises(
        ConstraintError, match="^Fixed size must be a positive integer"
    ):
        method_under_test(-14)


class TestFixedSizeMemoryUnitsCalculation:
    def test_none_constraint_asserts(self, elements_context):
        do_none_constraint_asserts(elements_context.constraint.calculate)

    def test_non_int_size_raises(self, elements_context):
        do_non_int_size_raises(elements_context.constraint.calculate)

    def test_negative_size_raises(self, elements_context):
        do_negative_size_raises(
            elements_context, elements_context.constraint.calculate
        )

    def test_over_size_raises(self, elements_context):
        expected_size = 12

        elements_context.constraint.value = expected_size

        with pytest.raises(ConstraintError, match="^Fixed size exceeded"):
            elements_context.constraint.calculate(14)

    def test_constraint_calculation(self, elements_context):
        expected_size = 12

        elements_context.constraint.value = expected_size

        actual_size = elements_context.constraint.calculate(10)

        assert actual_size == expected_size


class TestFixedSizeMemoryUnitsValidation:
    def test_non_int_size_raises(self, elements_context):
        do_non_int_size_raises(elements_context.constraint.validate)

    def test_negative_size_raises(self, elements_context):
        do_negative_size_raises(
            elements_context, elements_context.constraint.validate
        )


class TestFixedSizeMemoryUnitsValueProperty:
    def test_get_property(self, elements_context):
        expected_value = 14
        elements_context.constraint.value = expected_value

        actual_value = elements_context.constraint.value

        assert actual_value == expected_value

    def test_negative_constraint_raises(self, elements_context):
        with pytest.raises(
            ConstraintError, match="^Fixed size must be a positive integer"
        ):
            elements_context.constraint.value = -12


class TestLoadSave:
    def test_encode_decode(self, elements_context):
        elements_context.constraint.value = 4

        encoded_yaml_data = elements_context.constraint.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_data = FixedSizeMemoryUnits.from_yamlData(
            encoded_yaml_data, elements_context.memory
        )

        assert decoded_data.value == elements_context.constraint.value

    def test_decode_non_alignment_constraint_raises(self, elements_context):
        with pytest.raises(
            ParseError,
            match="^Yaml data does not contain fixed size constraint",
        ):
            FixedSizeMemoryUnits.from_yamlData({}, elements_context.memory)

    def test_decode_optional_alignment_constraint_raises(
        self, elements_context
    ):
        constraint = FixedAddress.from_yamlData(
            {}, elements_context.memory, optional=True
        )

        assert constraint is None
