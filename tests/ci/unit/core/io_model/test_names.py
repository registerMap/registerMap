#  Copyright (c) 2023 Russell Smiley
#
#  This file is part of registermap.
#
#  You should have received a copy of the GNU General Public License
#  along with registermap.  If not, see <http://www.gnu.org/licenses/>.

import re

import pydantic
import pytest

from registermap.core.io_model._names import (
    NAME_PATTERN,
    STRUCTURED_NAME_PATTERN,
    StructuredName,
)


@pytest.fixture()
def name_pattern():
    return re.compile(NAME_PATTERN)


@pytest.fixture()
def structured_name_pattern():
    return re.compile(STRUCTURED_NAME_PATTERN)


class TestNamePattern:
    def test_clean(self, name_pattern):
        good_names = [
            "m0",
            "a-bc0",
            "a_bc0",
            "m110",
            "a-b110",
            "a_b110",
            "abc0",
            "abc110",
            "ABC110",
            "AbC110",
        ]

        for this_name in good_names:
            if name_pattern.match(this_name) is None:
                pytest.fail(f"Expected good name failed, {this_name}")

    def test_bad(self, name_pattern):
        bad_names = [
            "a",
            "a-b",
            "0ab",
            "a&b",
        ]

        for this_name in bad_names:
            if name_pattern.match(this_name) is not None:
                pytest.fail(f"Expected bad name passed, {this_name}")


class TestStructuredNamePattern:
    def test_local_field(self, structured_name_pattern):
        result = structured_name_pattern.match("m0.r0.f0")
        assert result.group("module_global_field") == "m0"
        assert result.group("register") == "r0"
        assert result.group("field") == "f0"

    def test_register(self, structured_name_pattern):
        result = structured_name_pattern.match("m0.r0")
        assert result.group("module_global_field") == "m0"
        assert result.group("register") == "r0"
        assert result.group("field") is None

    def test_module_global_field(self, structured_name_pattern):
        result = structured_name_pattern.match("mgf0")
        assert result.group("module_global_field") == "mgf0"
        assert result.group("register") is None
        assert result.group("field") is None


class TestStructuredName:
    def test_module_global_field(self):
        under_test = StructuredName("mgf0")

        assert under_test.module_global_field == "mgf0"
        assert under_test.register is None
        assert under_test.field is None

    def test_register(self):
        under_test = StructuredName("m0.r3")

        assert under_test.module_global_field == "m0"
        assert under_test.register == "r3"
        assert under_test.field is None

    def test_local_field(self):
        under_test = StructuredName("m0.r3.f1")

        assert under_test.module_global_field == "m0"
        assert under_test.register == "r3"
        assert under_test.field == "f1"

    def test_to_str(self):
        expected_value = "m0.r3.f1"
        under_test = StructuredName(expected_value)

        assert str(under_test) == expected_value

    def test_pydantic_parse(self):
        ta = pydantic.TypeAdapter(StructuredName)

        result = ta.validate_python("m3.r1.f4")

        assert isinstance(result, StructuredName)
        assert result.module_global_field == "m3"
        assert result.register == "r1"
        assert result.field == "f4"
