#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

from registermap.core.interval import (
    ClosedIntegerInterval,
    any_overlap,
    is_encapsulated,
    is_overlap,
)


class TestIntervalOverlap:
    def test_upper_overlap1(self):
        # The first interval overlaps the upper edge of the second interval.
        i1 = ClosedIntegerInterval((4, 8))
        i2 = ClosedIntegerInterval((2, 4))

        assert is_overlap(i1, i2)

    def test_upper_overlap2(self):
        # The second interval overlaps the upper edge of the first interval.
        i1 = ClosedIntegerInterval((2, 4))
        i2 = ClosedIntegerInterval((4, 8))

        assert is_overlap(i1, i2)

    def test_lower_overlap1(self):
        # The first interval overlaps the lower edge of the second interval.
        i1 = ClosedIntegerInterval((2, 3))
        i2 = ClosedIntegerInterval((3, 6))

        assert is_overlap(i1, i2)

    def test_lower_overlap2(self):
        # The second interval overlaps the lower edge of the first interval.
        i1 = ClosedIntegerInterval((3, 6))
        i2 = ClosedIntegerInterval((2, 3))

        assert is_overlap(i1, i2)

    def test_no_overlap(self):
        # The intervals do not overlap.
        i1 = ClosedIntegerInterval((2, 3))
        i2 = ClosedIntegerInterval((4, 7))

        assert not is_overlap(i1, i2)

    def test_inside_overlap1(self):
        # The second interval is entirely inside the first interval.
        i1 = ClosedIntegerInterval((3, 9))
        i2 = ClosedIntegerInterval((4, 6))

        assert is_overlap(i1, i2)

    def test_inside_overlap2(self):
        # The first interval is entirely inside the second interval.
        i1 = ClosedIntegerInterval((4, 6))
        i2 = ClosedIntegerInterval((3, 9))

        assert is_overlap(i1, i2)

    def test_equal_overlap(self):
        # The first interval is equal to the second interval.
        i1 = ClosedIntegerInterval((4, 6))
        i2 = ClosedIntegerInterval((4, 6))

        assert is_overlap(i1, i2)


class TestIsEncapsulated:
    def test_is_encapsulated(self):
        # The first interval is entirely inside the second interval.
        i1 = ClosedIntegerInterval((4, 6))
        i2 = ClosedIntegerInterval((2, 9))

        assert is_encapsulated(i1, i2)

    def test_is_not_encapsulated(self):
        # The second interval is entirely inside the first interval.
        i1 = ClosedIntegerInterval((2, 9))
        i2 = ClosedIntegerInterval((4, 6))

        assert not is_encapsulated(i1, i2)

    def test_equal_overlap(self):
        # The first interval is equal to the second interval.
        i1 = ClosedIntegerInterval((4, 6))
        i2 = ClosedIntegerInterval((4, 6))

        assert is_encapsulated(i1, i2)


class TestAnyOverlap:
    def testOverlapTrue(self):
        input_value = [
            ClosedIntegerInterval((4, 6)),
            ClosedIntegerInterval((6, 8)),
        ]

        assert any_overlap(input_value)

    def test_overlap_true(self):
        input_value = [
            ClosedIntegerInterval((4, 6)),
            ClosedIntegerInterval((7, 8)),
        ]

        assert not any_overlap(input_value)
