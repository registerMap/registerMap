#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.core.observer import Observer
from registermap.structure.elements.base.parameter import Parameter


class MockObserver(Observer):
    def __init__(self):
        super().__init__()

        self.value = None

    def update(self, subject, arguments):
        self.value = subject.value


class TestObserveParameter:
    @pytest.fixture()
    def this_context(self):
        self.parameter_under_test = Parameter(value=6.6626)

        self.observer = MockObserver()

        self.parameter_under_test.addObserver(self.observer)

    def test_value_change_notifies(self, this_context):
        assert self.parameter_under_test.value != self.observer.value

        self.parameter_under_test.value = 3.14

        assert self.parameter_under_test.value == self.observer.value
