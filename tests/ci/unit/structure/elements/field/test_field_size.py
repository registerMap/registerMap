#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.field.field import Field

from ..mock_observer import MockObserver


class TestFieldSizeParameter:
    @pytest.fixture()
    def this_context(self):
        self.under_test = Field()

    def test_default_size(self, this_context):
        expected_size = 0

        actual_size = self.under_test["size"]

        assert actual_size == expected_size

    def test_assign_size(self, this_context):
        expected_size = 15

        assert self.under_test != expected_size

        self.under_test["size"] = expected_size
        actual_size = self.under_test["size"]

        assert actual_size == expected_size


class TestFieldSizeParameterNotification:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.under_test = Field()

        self.under_test.sizeChangeNotifier.addObserver(self.observer)

    def testSizeChangeNotification(self, this_context):
        expected_size = 15

        assert self.under_test != expected_size

        self.under_test["size"] = expected_size
        actual_size = self.under_test["size"]

        assert actual_size == expected_size
        assert self.observer.update_count == 1


class TestFieldSizeBitsMethod:
    @pytest.fixture()
    def this_context(self):
        self.under_test = Field()

    def test_default_size_bits(self, this_context):
        expected_size = 0

        actual_size = self.under_test.sizeBits

        assert actual_size == expected_size

    def test_assign_size(self, this_context):
        expected_size = 15

        assert self.under_test != expected_size

        self.under_test["size"] = expected_size
        actual_size_bits = self.under_test.sizeBits

        assert actual_size_bits == expected_size
