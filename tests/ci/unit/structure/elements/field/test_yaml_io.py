#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

import pytest

from registermap.structure.elements.field.field import Field, ParseError
from registermap.structure.elements.register import RegisterInstance

from ..mock_observer import MockObserver

log = logging.getLogger(__name__)


class TestLoadSaveGlobalField:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.test_field = Field()

        self.test_field.sizeChangeNotifier.addObserver(self.observer)

    def test_encode_decode_global_field(self, this_context):
        self.test_field["description"] = "some description"
        self.test_field["name"] = "f1"
        self.test_field["size"] = 8
        self.test_field["resetValue"] = 0x5A
        self.test_field["summary"] = "a summary"

        assert self.test_field["global"]

        encoded_yaml_data = self.test_field.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_data = Field.from_yamlData(encoded_yaml_data)

        assert decoded_data["description"] == self.test_field["description"]
        assert decoded_data["name"] == self.test_field["name"]
        assert decoded_data["resetValue"] == self.test_field["resetValue"]
        assert decoded_data["summary"] == self.test_field["summary"]
        assert decoded_data["global"]

    def test_encode_decode_local_field(self, this_context):
        self.test_field["description"] = "some description"
        self.test_field["name"] = "f1"
        self.test_field["size"] = 8
        self.test_field["resetValue"] = 0x5A
        self.test_field["summary"] = "a summary"

        assert self.test_field["global"]

        encoded_yaml_data = self.test_field.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_data = Field.from_yamlData(encoded_yaml_data)

        assert decoded_data["description"] == self.test_field["description"]
        assert decoded_data["name"] == self.test_field["name"]
        assert decoded_data["resetValue"] == self.test_field["resetValue"]
        assert decoded_data["summary"] == self.test_field["summary"]
        assert decoded_data["global"]

    def test_optional_description(self, this_context):
        self.test_field["name"] = "f1"
        self.test_field["size"] = 8
        self.test_field["resetValue"] = 0x5A
        self.test_field["summary"] = "a summary"

        encoded_yaml_data = self.test_field.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_data = Field.from_yamlData(encoded_yaml_data)

        assert decoded_data["description"] == ""

    def test_optional_summary(self, this_context):
        self.test_field["description"] = "some description"
        self.test_field["name"] = "f1"
        self.test_field["size"] = 8
        self.test_field["resetValue"] = 0x5A

        encoded_yaml_data = self.test_field.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_data = Field.from_yamlData(encoded_yaml_data)

        assert decoded_data["description"] == self.test_field["description"]
        assert decoded_data["name"] == self.test_field["name"]
        assert decoded_data["resetValue"] == self.test_field["resetValue"]
        assert decoded_data["summary"] == ""

    def test_field_larger_than_one_byte(self, this_context):
        self.test_field["name"] = "f1"
        self.test_field["size"] = 11

        encoded_yaml_data = self.test_field.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_data = Field.from_yamlData(encoded_yaml_data)

        assert decoded_data["name"] == self.test_field["name"]
        assert decoded_data["size"] == self.test_field["size"]


class TestLoadSaveLocalField:
    @pytest.fixture()
    def this_context(self, mocker):
        self.parent_register = self.generate_register("registerName", mocker)
        self.source_field = Field(parent=self.parent_register)

    @staticmethod
    def generate_register(name, this_mocker) -> RegisterInstance:
        """
        Generate mock parent register for the test.

        :param name:
        :return:
        """
        this_register = this_mocker.create_autospec(RegisterInstance)

        pn = this_mocker.PropertyMock(return_value=name)
        type(this_register).name = pn

        pci = this_mocker.PropertyMock(return_value=name)
        type(this_register).canonicalId = pci

        return this_register

    def test_encode_decode_local_field(self, this_context):
        self.source_field["name"] = "f1"
        self.source_field["size"] = 8
        self.source_field["resetValue"] = 0x5A

        assert not self.source_field["global"]
        assert self.source_field.canonicalId == "registerName.f1"

        encoded_yaml_data = self.source_field.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_data = Field.from_yamlData(
            encoded_yaml_data, parentRegister=self.parent_register
        )

        assert not decoded_data["global"]
        assert decoded_data.canonicalId == "registerName.f1"

    def test_decode_missing_parent_raises(self, this_context):
        self.source_field["name"] = "f1"
        self.source_field["size"] = 8
        self.source_field["resetValue"] = 0x5A

        assert not self.source_field["global"]
        assert self.source_field.canonicalId == "registerName.f1"

        encoded_yaml_data = self.source_field.to_yamlData()
        with pytest.raises(
            ParseError,
            match="^Parent register not specified for YAML acquisition of a local field",
        ):
            Field.from_yamlData(encoded_yaml_data)

    def test_decode_wrong_parent_raises(self, this_context, mocker):
        self.source_field["name"] = "f1"
        self.source_field["size"] = 8
        self.source_field["resetValue"] = 0x5A

        assert not self.source_field["global"]
        assert self.source_field.canonicalId == "registerName.f1"

        other_register = self.generate_register("other_register", mocker)

        encoded_yaml_data = self.source_field.to_yamlData()
        with pytest.raises(
            ParseError,
            match="^Parent register does not match YAML specification",
        ):
            Field.from_yamlData(
                encoded_yaml_data, parentRegister=other_register
            )


class TestLoadSaveUserDefinedParameters:
    @pytest.fixture()
    def this_context(self, mocker):
        self.parent_register = self.generate_register("registerName", mocker)
        self.source_field = Field(parent=self.parent_register)

    @staticmethod
    def generate_register(name, this_mocker) -> RegisterInstance:
        """
        Generate mock parent register for the test.

        :param name:
        :return:
        """
        this_register = this_mocker.create_autospec(RegisterInstance)

        pn = this_mocker.PropertyMock(return_value=name)
        type(this_register).name = pn

        pci = this_mocker.PropertyMock(return_value=name)
        type(this_register).canonicalId = pci

        return this_register

    def test_encode_decode_user_defined_parameters(self, this_context):
        expected_value = "some value"

        self.source_field["name"] = "f1"
        self.source_field["size"] = 8
        self.source_field["resetValue"] = 0x5A

        assert not self.source_field["global"]
        assert self.source_field.canonicalId == "registerName.f1"

        self.source_field["my-parameter"] = expected_value

        encoded_yaml_data = self.source_field.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_data = Field.from_yamlData(
            encoded_yaml_data, parentRegister=self.parent_register
        )

        assert expected_value == decoded_data["my-parameter"]
