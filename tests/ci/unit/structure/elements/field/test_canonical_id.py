#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.field import Field
from registermap.structure.elements.module import Module
from registermap.structure.elements.register import RegisterInstance
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set.fieldSet import FieldSet


class TestFieldCanonicalId:
    @pytest.fixture()
    def this_context(self):
        self.field_set = FieldSet()
        self.memory_space = MemoryConfiguration()

    def test_global_canonical_id(self, this_context):
        field_name = "thisField"

        test_field = Field()
        test_field["name"] = field_name

        expected_canonical_id = "{0}".format(field_name)

        assert test_field.canonicalId == expected_canonical_id
        assert test_field["global"]

    def test_local_canonical_id(self, this_context):
        field_name = "thisField"
        module_name = "thisModule"
        register_name = "thisRegister"

        parent_module = Module(self.memory_space, self.field_set)
        parent_module["name"] = module_name

        parent_register = RegisterInstance(
            self.memory_space,
            parent=parent_module,
            setCollection=self.field_set,
        )
        parent_register["name"] = register_name

        test_field = Field(parent=parent_register)
        test_field["name"] = field_name

        expected_canonical_id = "{0}.{1}.{2}".format(
            module_name, register_name, field_name
        )

        assert test_field.canonicalId == expected_canonical_id
        assert not test_field["global"]
