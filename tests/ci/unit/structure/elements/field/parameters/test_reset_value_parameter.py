#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.field.parameters import (
    ConfigurationError,
    ResetValueParameter,
)

from .test_int_parameter import TestPositiveIntParameter


class TestResetValueParameter(TestPositiveIntParameter):
    def type_under_test(self, value=0):
        return ResetValueParameter(value=value)

    @pytest.fixture()
    def this_context(self):
        self.parameter = self.type_under_test()

    def test_default_max_value(self, this_context):
        expected_value = 0

        actual_value = self.parameter.maxValue
        assert actual_value == expected_value


class TestResetValueParameterSize:
    def test_default_size(self):
        p = ResetValueParameter()

        expected_value = 0
        actual_value = p.size

        assert actual_value == expected_value

    def test_construct_size(self):
        expected_size = 2

        p = ResetValueParameter(size=expected_size)

        actual_size = p.size

        assert actual_size == expected_size

    def test_set_size(self):
        expected_size = 2

        p = ResetValueParameter()

        assert p.size != expected_size

        p.size = expected_size

        assert p.size == expected_size

    def test_negative_set_size_raises(self):
        p = ResetValueParameter()
        with pytest.raises(
            ConfigurationError, match="^Size must be positive int"
        ):
            p.size = -1

    def test_negative_construct_size_raises(self):
        with pytest.raises(
            ConfigurationError, match="^Size must be positive int"
        ):
            ResetValueParameter(size=-1)


class TestResetValueParameterMaxValue:
    def test_max_value(self):
        p = ResetValueParameter(size=2)

        assert p.maxValue == 3
