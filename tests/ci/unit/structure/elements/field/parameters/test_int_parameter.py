#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.field.parameters import (
    ConfigurationError,
    IntParameter,
    PositiveIntParameter,
)


class TestIntParameter:
    def type_under_test(self, value=0):
        """
        Children must implement this method to define the parameter type to apply testing to.

        :return: Type of parameter for testing.
        """
        return IntParameter("intParameter", value=value)

    def test_default_constructor_ok(self):
        self.type_under_test()

    def test_constructor_none_raises(self):
        with pytest.raises(ConfigurationError, match="^Must be an int"):
            self.type_under_test(value=None)

    def test_constructor_non_int_raises(self):
        with pytest.raises(ConfigurationError, match="^Must be an int"):
            self.type_under_test(value="30")

    def test_value_non_int_raises(self):
        p = self.type_under_test()
        with pytest.raises(ConfigurationError, match="^Must be an int"):
            p.value = list()


class TestPositiveIntParameter(TestIntParameter):
    def type_under_test(self, value=0):
        return PositiveIntParameter("positiveIntParameter", value=value)

    def test_negative_value_raises(self):
        p = self.type_under_test()
        with pytest.raises(ConfigurationError, match="^Must be a positive int"):
            p.value = -1
