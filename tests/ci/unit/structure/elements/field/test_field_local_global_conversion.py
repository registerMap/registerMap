#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.field.field import ConfigurationError
from registermap.structure.elements.register import RegisterInstance
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection


class TestConvertToGlobal:
    @pytest.fixture()
    def this_context(self):
        self.set_collection = SetCollection()
        self.memory = MemoryConfiguration()
        self.register = RegisterInstance(
            self.memory, setCollection=self.set_collection
        )

        self.test_field = self.register.addField("fieldName", [0, 4], (0, 4))

        assert not self.test_field["global"]

    def test_convert_local_to_global(self, this_context):
        self.test_field.convertToGlobal()

        assert self.test_field["global"]


class TestConvertToLocal:
    @pytest.fixture()
    def this_context(self):
        self.set_collection = SetCollection()
        self.memory = MemoryConfiguration()
        self.register = RegisterInstance(
            self.memory, setCollection=self.set_collection
        )

        self.test_field = self.register.addField(
            "fieldName", [0, 4], (0, 4), isGlobal=True
        )

        assert self.test_field["global"]

    def test_convert_global_to_local_single_register(self, this_context):
        self.test_field.convertToLocal(self.register)

        assert not self.test_field["global"]

    def test_multiple_registers_raises(self, this_context):
        register2 = RegisterInstance(
            self.memory, setCollection=self.set_collection
        )

        register2.addField("fieldName", [0, 2], (5, 7), isGlobal=True)

        with pytest.raises(
            ConfigurationError, match="^Field maps to multiple registers"
        ):
            self.test_field.convertToLocal(self.register)

    def test_absent_register(self, this_context):
        register2 = RegisterInstance(
            self.memory, setCollection=self.set_collection
        )

        new_field = register2.addField(
            "otherField", [0, 2], (5, 7), isGlobal=True
        )

        with pytest.raises(
            ConfigurationError,
            match="^Field does not map to the register selected for parent",
        ):
            new_field.convertToLocal(self.register)

    def test_force_single_parent(self, this_context):
        register2 = RegisterInstance(
            self.memory, setCollection=self.set_collection
        )

        register2.addField("fieldName", [0, 2], (5, 7), isGlobal=True)

        self.test_field.convertToLocal(self.register, removeOthers=True)

        assert not self.test_field["global"]
        assert register2 not in self.test_field.bitMap.destinations
