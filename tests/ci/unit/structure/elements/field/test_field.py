"""
Test BitField.
"""
#
# Copyright 2016 Russell Smiley
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

import pytest

from registermap.exceptions import ConfigurationError
from registermap.structure.elements.field.field import Field

from ..mock_observer import MockObserver

log = logging.getLogger(__name__)


class TestFieldDescription:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.test_field = Field()

        self.test_field.sizeChangeNotifier.addObserver(self.observer)

    def test_default_value(self, this_context):
        expected_value = ""
        assert self.test_field["description"] == expected_value

    def test_data_assignment(self, this_context):
        expected_value = "some description"
        assert expected_value != self.test_field["description"]

        self.test_field["description"] = expected_value

        assert self.test_field["description"] == expected_value
        assert self.observer.update_count == 0


class TestFieldGlobal:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.test_field = Field()

        self.test_field.sizeChangeNotifier.addObserver(self.observer)

    def test_default_value(self, this_context):
        assert self.test_field["global"]

    def test_data_assignment(self, this_context):
        class MockRegister:
            def __init__(self):
                self.canonicalId = "someName"

        register = MockRegister()

        test_field = Field(parent=register)

        assert not test_field["global"]


class TestFieldName:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.test_field = Field()

        self.test_field.sizeChangeNotifier.addObserver(self.observer)

    def test_default_value(self, this_context):
        expected_value = "unassigned"
        assert self.test_field["name"] == expected_value

    def test_data_assignment(self, this_context):
        expected_value = "new name"
        assert expected_value != self.test_field["name"]

        self.test_field["name"] = expected_value

        assert self.test_field["name"] == expected_value
        assert self.observer.update_count == 0


class TestFieldResetValue:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.test_field = Field()

        self.test_field.sizeChangeNotifier.addObserver(self.observer)

    def test_default_value(self, this_context):
        assert self.test_field["resetValue"] == 0

    def test_data_assignment(self, this_context):
        expected_value = 0xD
        self.test_field["size"] = 9
        assert self.observer.update_count == 1
        assert expected_value != self.test_field["resetValue"]

        self.test_field["resetValue"] = expected_value
        actual_value = self.test_field["resetValue"]

        assert actual_value == expected_value
        assert self.observer.update_count == 1

    def test_non_int_raises(self, this_context):
        with pytest.raises(ConfigurationError, match="^Must be an int"):
            self.test_field["resetValue"] = "5"

    def test_negative_int_raises(self, this_context):
        with pytest.raises(ConfigurationError, match="^Must be a positive int"):
            self.test_field["resetValue"] = -5

    def test_reset_value_greater_than_bit_range_raises(self, this_context):
        self.test_field["size"] = 2
        with pytest.raises(
            ConfigurationError,
            match="^Reset value cannot exceed number of bits of field",
        ):
            self.test_field["resetValue"] = 8

    def test_reset_value_assign_bit_range_undefined_raises(self, this_context):
        assert self.test_field["size"] == 0
        with pytest.raises(
            ConfigurationError,
            match="^Reset value cannot exceed number of bits of field",
        ):
            self.test_field["resetValue"] = 8


class TestFieldSize:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.test_field = Field()

        self.test_field.sizeChangeNotifier.addObserver(self.observer)

    def test_default_value(self, this_context):
        expected_value = 0

        assert self.test_field["size"] == expected_value
        assert self.test_field["size"] == self.test_field.sizeBits

    def test_data_assignment(self, this_context):
        expected_value = 10
        assert expected_value != self.test_field["size"]

        self.test_field["size"] = expected_value
        actual_value = self.test_field["size"]

        assert actual_value == expected_value
        assert self.observer.update_count == 1

    def test_changed_value_notifies(self, this_context):
        expected_value = 15
        assert self.test_field["size"] != expected_value

        self.test_field["size"] = expected_value

        assert self.observer.update_count == 1


class TestFieldSummary:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.test_field = Field()

        self.test_field.sizeChangeNotifier.addObserver(self.observer)

    def test_default_value(self, this_context):
        expected_value = ""
        assert self.test_field["summary"] == expected_value

    def test_data_assignment(self, this_context):
        expected_value = "does something"
        assert expected_value != self.test_field["summary"]

        self.test_field["summary"] = expected_value

        assert self.test_field["summary"] == expected_value
        assert self.observer.update_count == 0


class TestFieldParametersProperty:
    @pytest.fixture()
    def this_context(self):
        self.observer = MockObserver()
        self.test_field = Field()

        self.test_field.sizeChangeNotifier.addObserver(self.observer)

    def test_default_core_parameters_property(self, this_context):
        expected_value = {
            "name": "unassigned",
            "description": "",
            "global": True,
            "resetValue": 0,
            "size": 0,
            "summary": "",
        }

        actual_value = self.test_field.coreParameters

        assert expected_value == actual_value

    def test_default_user_parameters_property(self, this_context):
        expected_value = dict()

        actual_value = self.test_field.userParameters

        assert expected_value == actual_value

    def test_user_parameters_property(self, this_context):
        expected_value = {
            "someValue": 123,
        }

        self.test_field["someValue"] = 123

        actual_value = self.test_field.userParameters

        assert expected_value == actual_value
