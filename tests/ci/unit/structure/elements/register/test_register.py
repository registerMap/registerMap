#
# Copyright 2016 Russell Smiley
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Unit tests for Register.
"""

import logging

import pytest

from registermap.structure.elements.register.register import Register
from registermap.structure.memory import MemoryConfiguration
from registermap.structure.set import SetCollection

from .common import (
    CommonDescriptionParameterTests,
    CommonFieldIntervalTests,
    CommonFieldTests,
    CommonModeParameterTests,
    CommonNameParameterTests,
    CommonPublicParameterTests,
    CommonSizeBitsTests,
    CommonSummaryParameterTests,
    CommonUserDefinedParameterTests,
    CommonYamlIoTests,
)

log = logging.getLogger(__name__)


class TestRegisterDescription(
    CommonDescriptionParameterTests.TestRegisterDescription
):
    RegisterType = Register


class TestRegisterDefaultFieldInterval(
    CommonFieldIntervalTests.TestRegisterDefaultFieldInterval
):
    RegisterType = Register


class TestRegisterFields(CommonFieldTests.TestRegisterFields):
    RegisterType = Register


class TestFieldsMultipleRegisters(CommonFieldTests.TestFieldsMultipleRegisters):
    RegisterType = Register


class TestRegisterMode(CommonModeParameterTests.TestRegisterMode):
    RegisterType = Register


class TestRegisterName(CommonNameParameterTests.TestRegisterName):
    RegisterType = Register


class TestRegisterPublic(CommonPublicParameterTests.TestRegisterPublic):
    RegisterType = Register


class TestRegisterSize(CommonSizeBitsTests.TestRegisterSize):
    RegisterType = Register


class TestRegisterSizeBits(CommonSizeBitsTests.TestRegisterSizeBits):
    RegisterType = Register


class TestRegisterSummary(CommonSummaryParameterTests.TestRegisterSummary):
    RegisterType = Register


class TestRegisterUserDefinedParameter(
    CommonUserDefinedParameterTests.TestRegisterUserDefinedParameter
):
    RegisterType = Register


class TestRegisterYamlLoadSave(CommonYamlIoTests.TestRegisterYamlLoadSave):
    RegisterType = Register


class TestRegisterYamlLoadSaveCanonicalId(
    CommonYamlIoTests.TestRegisterYamlLoadSaveCanonicalId
):
    RegisterType = Register


class TestRegisterYamlParameters(CommonYamlIoTests.TestRegisterYamlParameters):
    RegisterType = Register


class TestLoadSaveUserDefinedParameter(
    CommonYamlIoTests.TestLoadSaveUserDefinedParameter
):
    RegisterType = Register


class TestFieldParametersProperty:
    @pytest.fixture()
    def this_context(self):
        self.mock_memoryConfiguration = MemoryConfiguration()
        self.mock_set = SetCollection()

        self.under_test = Register(self.mock_memoryConfiguration, self.mock_set)

    def test_default_core_parameters_property(self, this_context):
        expected_value = {
            "constraints": self.under_test["constraints"],
            "description": "",
            "fields": self.under_test["fields"],
            "global": True,
            "mode": "rw",
            "name": None,
            "public": True,
            "summary": "",
        }

        actual_value = self.under_test.coreParameters

        assert expected_value == actual_value

    def test_default_user_parameters_property(self, this_context):
        expected_value = dict()

        actual_value = self.under_test.userParameters

        assert expected_value == actual_value

    def test_user_parameters_property(self, this_context):
        expected_value = {
            "someValue": 123,
        }

        self.under_test["someValue"] = 123

        actual_value = self.under_test.userParameters

        assert expected_value == actual_value
