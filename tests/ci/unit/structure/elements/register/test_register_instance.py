#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

from registermap.structure.elements.register.instance import RegisterInstance

from .common import (
    CommonDescriptionParameterTests,
    CommonFieldIntervalTests,
    CommonFieldTests,
    CommonModeParameterTests,
    CommonNameParameterTests,
    CommonPublicParameterTests,
    CommonSizeBitsTests,
    CommonSummaryParameterTests,
    CommonUserDefinedParameterTests,
    CommonYamlIoTests,
)


class TestRegisterDescription(
    CommonDescriptionParameterTests.TestRegisterDescription
):
    RegisterType = RegisterInstance


class TestRegisterDefaultFieldInterval(
    CommonFieldIntervalTests.TestRegisterDefaultFieldInterval
):
    RegisterType = RegisterInstance


class TestRegisterFields(CommonFieldTests.TestRegisterFields):
    RegisterType = RegisterInstance


class TestFieldsMultipleRegisters(CommonFieldTests.TestFieldsMultipleRegisters):
    RegisterType = RegisterInstance


class TestRegisterMode(CommonModeParameterTests.TestRegisterMode):
    RegisterType = RegisterInstance


class TestRegisterName(CommonNameParameterTests.TestRegisterName):
    RegisterType = RegisterInstance


class TestRegisterPublic(CommonPublicParameterTests.TestRegisterPublic):
    RegisterType = RegisterInstance


class TestRegisterSize(CommonSizeBitsTests.TestRegisterSize):
    RegisterType = RegisterInstance


class TestRegisterSizeBits(CommonSizeBitsTests.TestRegisterSizeBits):
    RegisterType = RegisterInstance


class TestRegisterSummary(CommonSummaryParameterTests.TestRegisterSummary):
    RegisterType = RegisterInstance


class TestRegisterUserDefinedParameter(
    CommonUserDefinedParameterTests.TestRegisterUserDefinedParameter
):
    RegisterType = RegisterInstance


class TestRegisterYamlLoadSave(CommonYamlIoTests.TestRegisterYamlLoadSave):
    RegisterType = RegisterInstance


class TestRegisterYamlLoadSaveCanonicalId(
    CommonYamlIoTests.TestRegisterYamlLoadSaveCanonicalId
):
    RegisterType = RegisterInstance


class TestRegisterYamlParameters(CommonYamlIoTests.TestRegisterYamlParameters):
    RegisterType = RegisterInstance


class TestLoadSaveUserDefinedParameter(
    CommonYamlIoTests.TestLoadSaveUserDefinedParameter
):
    RegisterType = RegisterInstance
