#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

import pytest

from registermap.exceptions import ParseError
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection

from ...mock_observer import MockObserver
from ..mocks import MockPreviousRegister

log = logging.getLogger(__name__)


class MockModule:
    def __init__(self, name):
        self.__name = name

    def __getitem__(self, item):
        assert item == "name"

        return self.__name


class CommonYamlIoTests:
    class TestRegisterYamlLoadSave:
        # The tests will fail unless a test case loader correctly fulfills this value.
        RegisterType = None

        @pytest.fixture()
        def this_context(self):
            self.source_collection = SetCollection()
            self.test_space = MemoryConfiguration()
            self.under_test = self.RegisterType(
                self.test_space, setCollection=self.source_collection
            )

            self.observer = MockObserver()
            self.under_test.sizeChangeNotifier.addObserver(self.observer)

            self.acquired_collection = SetCollection()

        def test_encode_decode(self, this_context):
            def check_fields():
                """
                Test the register fields have been recovered correctly.
                """
                nonlocal self, decoded_register

                assert (
                    decoded_register["fields"]["f1"]["name"]
                    == self.under_test["fields"]["f1"]["name"]
                )
                assert (
                    decoded_register["fields"]["f1"]["size"]
                    == self.under_test["fields"]["f1"]["size"]
                )
                assert (
                    decoded_register["fields"]["f2"]["name"]
                    == self.under_test["fields"]["f2"]["name"]
                )
                assert (
                    decoded_register["fields"]["f2"]["size"]
                    == self.under_test["fields"]["f2"]["size"]
                )

                # Ensure that acquired fields have been added to the field set.
                assert len(self.source_collection.fieldSet) == len(
                    self.under_test["fields"]
                )
                field_set_ids = [
                    x.canonicalId for x in self.source_collection.fieldSet
                ]
                for this_field in decoded_register["fields"].values():
                    assert this_field.canonicalId in field_set_ids

            def check_parameters():
                """
                Test the register parameters, other than 'fields', have been recovered correctly.
                """
                nonlocal self, decoded_register

                assert (
                    decoded_register["constraints"]["fixedAddress"]
                    == self.under_test["constraints"]["fixedAddress"]
                )
                assert (
                    decoded_register["description"]
                    == self.under_test["description"]
                )
                assert decoded_register["mode"] == self.under_test["mode"]
                assert decoded_register["name"] == self.under_test["name"]
                assert decoded_register["public"] == self.under_test["public"]
                assert decoded_register["summary"] == self.under_test["summary"]

            def check_bitmap():
                nonlocal self, decoded_register

                for this_field in self.acquired_collection.fieldSet:
                    # Assume that this test indicates the bitmap has acquired itself from YAML correctly;
                    # other bit map specific tests will exhaustively test bit map YAML acquisition.
                    assert this_field in decoded_register.bitMap.destinations

                    # Check that the reciprocal map has been established.
                    assert (
                        decoded_register.bitMap.source
                        in this_field.bitMap.destinations
                    )

            self.under_test["constraints"]["fixedAddress"] = 0x10
            self.under_test["description"] = "some description"
            self.under_test["mode"] = "ro"
            self.under_test["name"] = "registerName"
            self.under_test["public"] = False
            self.under_test["summary"] = "a summary"

            self.under_test.addField("f1", [3, 5], (0, 2))
            self.under_test.addField("f2", [7, 7], (0, 0))

            assert self.under_test.canonicalId == self.under_test["name"]

            encoded_yaml_data = self.under_test.to_yamlData()
            log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
            decoded_register = self.RegisterType.from_yamlData(
                encoded_yaml_data, self.test_space, self.acquired_collection
            )

            check_fields()
            check_parameters()
            check_bitmap()

        def test_default_encode_decode(self, this_context):
            encoded_yaml_data = self.under_test.to_yamlData()
            log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
            decoded_register = self.RegisterType.from_yamlData(
                encoded_yaml_data, self.test_space, self.source_collection
            )

            assert len(decoded_register["fields"]) == 0
            assert len(decoded_register["constraints"]) == 0
            assert decoded_register["description"] == ""
            assert decoded_register["mode"] == "rw"
            assert decoded_register["name"] is None
            assert decoded_register["public"] == True
            assert decoded_register["summary"] == ""

        def test_bad_yaml_data_raises(self, this_context):
            yaml_data = {"mode": "ro"}
            with pytest.raises(
                ParseError, match="^Yaml data does not specify register"
            ):
                self.RegisterType.from_yamlData(
                    yaml_data,
                    self.test_space,
                    self.source_collection,
                    optional=False,
                )

        def test_optional_yaml_data(self, this_context):
            # Specifying an optional YAML decoding when a register YAML encoding is not present must result in a Register
            # populated with default values.
            yaml_data = {"mode": "ro"}
            decoded_register = self.RegisterType.from_yamlData(
                yaml_data,
                self.test_space,
                self.acquired_collection,
                optional=True,
            )
            assert len(decoded_register["fields"]) == 0
            assert len(decoded_register["constraints"]) == 0
            assert decoded_register["description"] == ""
            assert decoded_register["mode"] == "rw"
            assert decoded_register["name"] is None
            assert decoded_register["public"] == True
            assert decoded_register["summary"] == ""

        def test_register_with_field_larger_than_one_byte(self, this_context):
            expected_register_size_bits = 16

            self.under_test["name"] = "registerName"

            self.under_test.addField("f1", (0, 11))

            assert expected_register_size_bits == self.under_test.sizeBits

            assert self.under_test.canonicalId == self.under_test["name"]

            encoded_yaml_data = self.under_test.to_yamlData()
            log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
            decoded_register = self.RegisterType.from_yamlData(
                encoded_yaml_data, self.test_space, self.acquired_collection
            )

            assert self.under_test["name"] == decoded_register["name"]
            assert expected_register_size_bits == decoded_register.sizeBits

        def test_register_with_multiples_fields_across_byte_boundary(
            self, this_context
        ):
            expected_register_size_bits = 16

            self.under_test["name"] = "registerName"

            # This field allocation implies a two byte register required to encapsulate them.
            self.under_test.addField("f1", (3, 5))
            self.under_test.addField("f2", (10, 11))

            assert expected_register_size_bits == self.under_test.sizeBits

            assert self.under_test.canonicalId == self.under_test["name"]

            encoded_yaml_data = self.under_test.to_yamlData()
            log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
            decoded_register = self.RegisterType.from_yamlData(
                encoded_yaml_data, self.test_space, self.acquired_collection
            )

            assert self.under_test["name"] == decoded_register["name"]
            assert expected_register_size_bits == decoded_register.sizeBits

    class TestRegisterYamlParameters:
        # The tests will fail unless a test case loader correctly fulfills this value.
        RegisterType = None

        @pytest.fixture()
        def this_context(self):
            self.set_collection = SetCollection()
            self.test_space = MemoryConfiguration()
            self.under_test = self.RegisterType(
                self.test_space, setCollection=self.set_collection
            )

            self.previous_register = MockPreviousRegister(
                endAddress=0x3E7, sizeMemoryUnits=4
            )
            self.under_test.previousElement = self.previous_register

            self.observer = MockObserver()
            self.under_test.sizeChangeNotifier.addObserver(self.observer)

        def test_yaml_data_span(self, this_context):
            # The address data is automatically generated so it is prefixed by '_'.
            expected_name = "_sizeMemoryUnits"
            expected_value = 1

            assert expected_value == self.under_test.sizeMemoryUnits

            yaml_data = self.under_test.to_yamlData()
            assert expected_value == yaml_data["register"][expected_name]

    class TestRegisterYamlLoadSaveCanonicalId:
        # The tests will fail unless a test case loader correctly fulfills this value.
        RegisterType = None

        @pytest.fixture()
        def this_context(self):
            self.source_collection = SetCollection()
            self.space = MemoryConfiguration()
            self.module = MockModule("module")

            self.acquired_collection = SetCollection()

        def test_encode_decode(self, this_context):
            """
            The canonical ID of the decoded register much match that of the encoded register and is expected to include the
            module canonical ID.
            :return:
            """
            test_register = self.RegisterType(
                self.space,
                parent=self.module,
                setCollection=self.source_collection,
            )
            test_register["name"] = "register"

            self.source_collection.registerSet.add(test_register)

            assert "module.register" == test_register.canonicalId

            encoded_yaml_data = test_register.to_yamlData()
            log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
            decoded_register = self.RegisterType.from_yamlData(
                encoded_yaml_data,
                self.space,
                self.acquired_collection,
                parent=self.module,
            )

            assert test_register.canonicalId == decoded_register.canonicalId
            assert "module.register" == test_register.canonicalId

    class TestLoadSaveUserDefinedParameter:
        # The tests will fail unless a test case loader correctly fulfills this value.
        RegisterType = None

        @pytest.fixture()
        def this_context(self):
            self.source_collection = SetCollection()
            self.testSpace = MemoryConfiguration()
            self.under_test = self.RegisterType(
                self.testSpace, setCollection=self.source_collection
            )

            self.observer = MockObserver()
            self.under_test.sizeChangeNotifier.addObserver(self.observer)

            self.acquired_collection = SetCollection()

        def test_encode_decode(self, this_context):
            expected_value = "some value"

            self.under_test["my-parameter"] = expected_value

            encoded_yaml_data = self.under_test.to_yamlData()
            log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
            decoded_register = self.RegisterType.from_yamlData(
                encoded_yaml_data, self.testSpace, self.acquired_collection
            )

            assert expected_value == decoded_register["my-parameter"]
