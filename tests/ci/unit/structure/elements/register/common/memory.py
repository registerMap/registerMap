#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#


import pytest

from ...memory.configuration import MemoryConfiguration
from ...set import SetCollection


class CommonMemoryPropertyTests:
    class TestRegisterMemoryProperty:
        # The tests will fail unless a test case loader correctly fulfills this value.
        RegisterType = None

        @pytest.fixture()
        def this_context(self):
            self.setCollection = SetCollection()
            self.testSpace = MemoryConfiguration()
            self.registerUnderTest = self.RegisterType(
                self.testSpace, setCollection=self.setCollection
            )

        def testMemoryProperty(self, this_context):
            assert self.testSpace == self.registerUnderTest.memory
