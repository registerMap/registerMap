#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import collections
import logging

import pytest

from registermap.exceptions import ConfigurationError
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection

from ...mock_observer import MockObserver

log = logging.getLogger(__name__)


class CommonFieldTests:
    class TestRegisterFields:
        # The tests will fail unless a test case loader correctly fulfills this value.
        RegisterType = None

        @pytest.fixture()
        def this_context(self):
            self.observer = MockObserver()
            self.set_collection = SetCollection()
            self.test_space = MemoryConfiguration()

            self.under_test = self.RegisterType(
                self.test_space, setCollection=self.set_collection
            )

            self.under_test.sizeChangeNotifier.addObserver(self.observer)

        def test_default_value(self, this_context):
            assert self.under_test["fields"] == collections.OrderedDict()

        def test_multiple_fields_added_to_arbitrary_size_without_constraint(
            self, this_context
        ):
            assert self.test_space.memoryUnitBits == 8
            assert len(self.under_test["fields"]) == 0

            self.under_test.addField("testField1", [0, 3], (0, 3))
            self.under_test.addField("testField2", [4, 10], (0, 6))
            self.under_test.addField("testField3", [11, 25], (0, 14))

            assert self.under_test.sizeMemoryUnits == 4

        def test_overlapping_register_interval_raises(self, this_context):
            self.under_test.addField("field1", [0, 5], (0, 5))
            assert len(self.under_test["fields"]) == 1

            with pytest.raises(
                ConfigurationError,
                match="^Specifed source interval overlaps existing source intervals",
            ):
                self.under_test.addField("field2", [5, 6], (0, 1))

        def test_overlapping_field_interval_raises(self, this_context):
            self.under_test.addField("field1", [0, 5], (0, 5))
            assert len(self.under_test["fields"]) == 1

            with pytest.raises(
                ConfigurationError,
                match="^Specifed destination interval overlaps existing destination intervals",
            ):
                self.under_test.addField("field1", [6, 7], (5, 6))

    class TestFieldsMultipleRegisters:
        # The tests will fail unless a child class correctly fulfills this value.:
        RegisterType = None

        @pytest.fixture()
        def this_context(self):
            self.set_collection = SetCollection()
            self.test_space = MemoryConfiguration()

        def test_add_local_fields_to_different_registers(self, this_context):
            assert len(self.set_collection.fieldSet) == 0

            register1 = self.RegisterType(
                self.test_space, setCollection=self.set_collection
            )
            register1.addField("field1", [5, 6], (0, 1))

            assert len(self.set_collection.fieldSet) == 1
            assert list(self.set_collection.fieldSet)[0]["name"] == "field1"

            register2 = self.RegisterType(
                self.test_space, setCollection=self.set_collection
            )
            register2.addField("field1", [3, 5], (4, 6))

            assert len(self.set_collection.fieldSet) == 2
            assert list(self.set_collection.fieldSet)[0]["name"] == "field1"
            assert list(self.set_collection.fieldSet)[1]["name"] == "field1"

        def test_add_local_fields_to_same_registers(self, this_context):
            assert len(self.set_collection.fieldSet) == 0

            register1 = self.RegisterType(
                self.test_space, setCollection=self.set_collection
            )
            register1.addField("field1", [5, 6], (0, 1))

            assert len(self.set_collection.fieldSet) == 1
            assert list(self.set_collection.fieldSet)[0]["name"] == "field1"

            register1.addField("field1", [2, 4], (4, 6))

            assert len(self.set_collection.fieldSet) == 1
            assert list(self.set_collection.fieldSet)[0]["name"] == "field1"

        def test_add_global_field_to_registers(self, this_context):
            assert len(self.set_collection.fieldSet) == 0

            register1 = self.RegisterType(
                self.test_space, setCollection=self.set_collection
            )
            register1.addField("field1", [5, 6], (0, 1), isGlobal=True)

            assert len(self.set_collection.fieldSet) == 1
            assert list(self.set_collection.fieldSet)[0]["name"] == "field1"

            register2 = self.RegisterType(
                self.test_space, setCollection=self.set_collection
            )
            register2.addField("field1", [2, 4], (2, 4), isGlobal=True)

            # The bit field set should not have changed since the initial bitfield was defined as global.
            assert len(self.set_collection.fieldSet) == 1
