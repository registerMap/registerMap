#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.exceptions import ConfigurationError
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection

from ...mock_observer import MockObserver


class CommonPublicParameterTests:
    class TestRegisterPublic:
        # The tests will fail unless a test case loader correctly fulfills this value.
        RegisterType = None

        @pytest.fixture()
        def this_context(self):
            self.observer = MockObserver()
            self.set_collection = SetCollection()
            self.test_space = MemoryConfiguration()
            self.under_test = self.RegisterType(
                self.test_space, setCollection=self.set_collection
            )

            self.under_test.sizeChangeNotifier.addObserver(self.observer)

        def test_default_value(self, this_context):
            expected_value = True
            assert self.under_test["public"] == expected_value

        def test_data_assignment(self, this_context):
            assert self.under_test["public"]

            self.under_test["public"] = False

            assert not self.under_test["public"]
            assert self.observer.update_count == 0

        def test_non_bool_raises(self, this_context):
            with pytest.raises(
                ConfigurationError, match="^Public must be specified as boolean"
            ):
                self.under_test["public"] = "true"
