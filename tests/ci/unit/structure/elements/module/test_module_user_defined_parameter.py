#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.module.module import Module
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection


class TestModuleUserDefinedParameter:
    @pytest.fixture()
    def this_context(self):
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()

        self.under_test = Module(self.test_space, self.set_collection)

    def test_assign_user_parameter_ok(self, this_context):
        expected_value = "some value"

        self.under_test["my-parameter"] = expected_value

        assert expected_value == self.under_test["my-parameter"]

    def test_bad_parameter_raises(self, this_context):
        with pytest.raises(
            KeyError, match="Module parameter not in core or user data"
        ):
            self.under_test["bad-parameter"]

    def test_underscore_prefix_asserts(self, this_context):
        with pytest.raises(AssertionError):
            self.under_test["_my-parameter"] = 2
