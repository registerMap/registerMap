#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.module import Module
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set.fieldSet import FieldSet


class TestModuleCananicalId:
    @pytest.fixture()
    def this_context(self):
        self.field_set = FieldSet()
        self.memory_space = MemoryConfiguration()

        self.under_test = Module(self.memory_space, self.field_set)

    def test_canonical_id_okay(self, this_context):
        expected_name = "someName"

        self.under_test["name"] = expected_name

        assert self.under_test.canonicalId == expected_name
