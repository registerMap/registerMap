#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.module.instance import ModuleInstance
from registermap.structure.elements.module.module import Module

from .common import CommonModuleInterfaceTests


class TestModuleInstance:
    @pytest.fixture()
    def this_context(self, mocker):
        self.mock_memory = mocker.MagicMock()

        self.mock_module_parent = mocker.create_autospec(Module)
        self.mock_module_parent.canonicalId = "one.two"
        self.mock_module_parent.memory = self.mock_memory

        self.under_test = ModuleInstance(self.mock_module_parent)
        self.under_test["name"] = "m1"

    def test_assigned_memory_units_property(self, this_context):
        self.mock_module_parent.assignedMemoryUnits = 5

        assert (
            self.mock_module_parent.assignedMemoryUnits
            == self.under_test.assignedMemoryUnits
        )

    def test_canonical_id_property(self, this_context, mocker):
        self.mock_previousElement = mocker.MagicMock()
        self.mock_previousElement.endAddress = 0x100

        self.under_test.previousElement = self.mock_previousElement

        assert 0x101 == self.under_test.baseAddress

        assert "one.two[0x101]" == self.under_test.canonicalId

    def test_memory_property(self, this_context):
        assert self.mock_memory == self.under_test.memory


class TestModuleInstanceBaseAddressProperty:
    @pytest.fixture()
    def this_context(self, mocker):
        self.mock_memory = mocker.MagicMock()

        self.mock_previous_element = mocker.MagicMock()
        self.mock_previous_element.endAddress = None

        self.mock_module_parent = mocker.create_autospec(Module)
        self.mock_module_parent.canonicalId = "one.two"
        self.mock_module_parent.memory = self.mock_memory

        self.under_test = ModuleInstance(self.mock_module_parent)
        self.under_test["name"] = "m1"
        self.under_test.previousElement = self.mock_previous_element

    def test_default(self, this_context):
        """
        With no constraints and no previous element specified, the base address can only be `None`.
        """
        assert self.under_test.baseAddress is None

    def test_no_constraints_none_previous_end_address(self, this_context):
        """
        With no constraints applied and a previous element with undefined end address, the base address can only be
        `None`.
        """
        self.mock_previous_element.endAddress = None
        self.under_test.previousElement = self.mock_previous_element

        assert self.under_test.baseAddress is None

    def test_no_constraints_previous_end_address_defined(self, this_context):
        """
        With no constraints applied and a previous element with defined end address, the base address can only be the
        next address.
        """
        self.mock_previous_element.endAddress = 0x100

        expected_value = self.mock_previous_element.endAddress + 1

        assert expected_value == self.under_test.baseAddress


class TestModuleInstanceConstraints(
    CommonModuleInterfaceTests.TestModuleConstraints
):
    def construct_instance_under_test(self):
        import unittest.mock

        self.mock_module_parent = unittest.mock.create_autospec(Module)
        self.mock_module_parent.canonicalId = "one.two"
        self.mock_module_parent.memory = self.mock_memory

        element_under_test = ModuleInstance(self.mock_module_parent)
        element_under_test["name"] = "m1"

        return element_under_test


class TestModuleInstanceOffsetProperty(
    CommonModuleInterfaceTests.TestModuleOffsetProperty
):
    def construct_instance_under_test(self):
        import unittest.mock

        self.mock_module_parent = unittest.mock.create_autospec(Module)
        self.mock_module_parent.canonicalId = "one.two"
        self.mock_module_parent.memory = self.mock_memory

        module_under_test = ModuleInstance(self.mock_module_parent)
        module_under_test["name"] = "m1"

        return module_under_test


class TestModuleInstancePreviousElementProperty(
    CommonModuleInterfaceTests.TestModulePreviousElementProperty
):
    def construct_instance_under_test(self):
        import unittest.mock

        self.mock_module_parent = unittest.mock.create_autospec(Module)
        self.mock_module_parent.memory = self.mock_memory

        element_under_test = ModuleInstance(self.mock_module_parent)
        element_under_test["name"] = "m1"

        return element_under_test


class TestModuleNameParameter(
    CommonModuleInterfaceTests.TestModuleNameParameter
):
    def construct_instance_under_test(self):
        self.mock_module_parent = Module(self.mock_memory, self.set_collection)

        element_under_test = ModuleInstance(self.mock_module_parent)

        return element_under_test

    def test_parent_name_assignment(self, this_context):
        """
        A name assigned to a module instance assigns the name to the parent module.
        """
        expected_value = "some-name"

        self.under_test["name"] = expected_value

        assert expected_value == self.under_test["name"]
        assert expected_value == self.mock_module_parent["name"]
