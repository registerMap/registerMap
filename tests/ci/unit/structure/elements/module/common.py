#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import abc

import pytest

from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection

from .mocks import MockPreviousModule


class CommonModuleInterfaceTests:
    class CommonTestInterface(metaclass=abc.ABCMeta):
        @abc.abstractmethod
        def construct_instance_under_test(self):
            """
                        Instantiate the module class object to be tested.
            :
                        :return: Instance of module class under test.:
            """
            pass

    class TestModuleConstraints(metaclass=abc.ABCMeta):
        @abc.abstractmethod
        def construct_instance_under_test(self):
            """
                        Instantiate the module class object to be tested.
            :
                        :return: Instance of module class under test.:
            """
            pass

        @pytest.fixture()
        def this_context(self):
            self.mock_memory = MemoryConfiguration()
            self.mock_setCollection = SetCollection()

            self.mock_previousElement = MockPreviousModule(endAddress=None)

            self.under_test = self.construct_instance_under_test()

            self.under_test.previousElement = self.mock_previousElement

        def test_fixed_address(self, this_context):
            self.mock_previousElement.endAddress = 0x10

            expected_value = 0x15

            assert expected_value > self.mock_previousElement.endAddress
            assert expected_value != self.under_test.baseAddress

            self.under_test["constraints"]["fixedAddress"] = expected_value

            assert expected_value == self.under_test.baseAddress

        def test_aligned_address(self, this_context):
            self.mock_previousElement.endAddress = 0x10

            alignment_value = 2
            expected_value = self.mock_previousElement.endAddress + 2

            assert 0 == (expected_value % alignment_value)
            assert self.under_test.baseAddress < expected_value

            assert expected_value != self.under_test.baseAddress

            self.under_test["constraints"][
                "alignmentMemoryUnits"
            ] = alignment_value

            assert expected_value == self.under_test.baseAddress

        def test_fixed_address_constraint_previous_element_none(
            self, this_context
        ):
            """
            Fixed address constraint returns None if the previous element end address is None.
            """
            self.mock_previousElement.endAddress = None

            expected_value = 0x500

            assert expected_value != self.under_test.baseAddress

            self.under_test["constraints"]["fixedAddress"] = expected_value

            assert expected_value == self.under_test.baseAddress

    class TestModuleOffsetProperty(CommonTestInterface):
        @pytest.fixture()
        def this_context(self):
            self.set_collection = SetCollection()
            self.mock_memory = MemoryConfiguration()
            self.mock_memory.baseAddress = 0x100

            self.mock_previousElement = MockPreviousModule(endAddress=0xFF)

            self.under_test = self.construct_instance_under_test()

            self.under_test.previousElement = self.mock_previousElement

        def test_default(self, this_context):
            self.under_test["constraints"][
                "fixedAddress"
            ] = self.mock_memory.baseAddress

            assert self.mock_memory.baseAddress == self.under_test.baseAddress
            assert 0 == self.under_test.offset

        def test_address_changed(self, this_context):
            expected_address = 0x300
            expected_offset = expected_address - self.mock_memory.baseAddress

            self.under_test["constraints"]["fixedAddress"] = expected_address

            assert expected_address == self.under_test.baseAddress
            assert expected_offset == self.under_test.offset

        def test_offset_property(self, this_context):
            self.mock_previousElement.endAddress = 0x1FF

            expected_offset = 0x100
            expected_address = 0x200

            assert expected_offset == self.under_test.offset
            assert expected_address == self.under_test.baseAddress

    class TestModulePreviousElementProperty:
        @pytest.fixture()
        def this_context(self):
            self.set_collection = SetCollection()
            self.mock_memory = MemoryConfiguration()
            self.mock_memory.baseAddress = 0x100

            self.mock_previousElement = MockPreviousModule(endAddress=0xFF)

            self.under_test = self.construct_instance_under_test()

        def test_previous_element_assignment(self, this_context):
            self.under_test.previousElement = self.mock_previousElement

            assert self.mock_previousElement == self.under_test.previousElement

        def testDefaultValue(self, this_context):
            assert self.under_test.previousElement is None

        def test_assign_previous_module_none_end_address(self, this_context):
            previous_module = MockPreviousModule()
            self.under_test.previousElement = previous_module

            assert self.under_test.endAddress is None

        def test_update_previous_module_end_address(self, this_context):
            previous_module = MockPreviousModule()
            self.under_test.previousElement = previous_module

            expected_address = 0x10
            previous_module.endAddress = expected_address - 1

            assert expected_address == self.under_test.baseAddress

    class TestModuleNameParameter:
        @pytest.fixture()
        def this_context(self):
            self.set_collection = SetCollection()
            self.mock_memory = MemoryConfiguration()

            self.under_test = self.construct_instance_under_test()

        def test_default_value(self, this_context):
            assert self.under_test["name"] is None

        def test_name_assignment(self, this_context):
            expected_value = "some-name"

            self.under_test["name"] = expected_value

            assert expected_value == self.under_test["name"]
