#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

from registermap.core.observable import Observable


class MockPreviousModule:
    def __init__(self, endAddress=None):
        self.addressChangeNotifier = Observable()
        self.sizeChangeNotifier = Observable()

        self.__endAddress = endAddress

    @property
    def endAddress(self):
        return self.__endAddress

    @endAddress.setter
    def endAddress(self, value):
        self.__endAddress = value
        self.sizeChangeNotifier.notifyObservers()
