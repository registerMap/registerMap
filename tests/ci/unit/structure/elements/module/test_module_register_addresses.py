#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.module.module import (
    ConfigurationError,
    Module,
)
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set import SetCollection

from ..mock_observer import MockObserver
from .mocks import MockPreviousModule


class TestModuleAddRegisterNoneAddresses:
    @pytest.fixture()
    def this_context(self):
        self.previous_module = MockPreviousModule(endAddress=None)
        self.observer = MockObserver()
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)

        self.under_test.previousElement = self.previous_module

        self.under_test.sizeChangeNotifier.addObserver(self.observer)
        self.under_test.addressChangeNotifier.addObserver(self.observer)

    def test_add_single_register(self, this_context):
        expected_name = "r1"

        assert 0 == len(self.under_test["registers"])

        added_register = self.under_test.addRegister(expected_name)

        assert 1 == len(self.under_test["registers"])
        assert (
            expected_name == self.under_test["registers"][expected_name]["name"]
        )
        # The addRegister method returns the created register
        assert added_register == self.under_test["registers"][expected_name]

        assert added_register.startAddress is None

    def test_add_duplicate_register_raises(self, this_context):
        # A register name is unique to its module
        expected_name = "r1"
        self.under_test.addRegister(expected_name)
        with pytest.raises(
            ConfigurationError,
            match="^Created register names must be unique within a module",
        ):
            self.under_test.addRegister(expected_name)

    def test_add_multiple_registers_with_subsequent_concrete_address(
        self, this_context
    ):
        r1 = self.under_test.addRegister("r1")
        assert r1.startAddress is None
        r2 = self.under_test.addRegister("r2")
        assert r2.startAddress is None

        previous_module = MockPreviousModule(endAddress=0x10)

        self.under_test.previousElement = previous_module

        assert 0x11 == self.under_test.baseAddress
        assert 0x11 == r1.startAddress
        assert 0x12 == r2.startAddress


class TestModuleAddRegisterConcreteAddresses:
    @pytest.fixture()
    def this_context(self):
        self.previous_module = MockPreviousModule(endAddress=0x10)
        self.observer = MockObserver()
        self.set_collection = SetCollection()
        self.test_space = MemoryConfiguration()
        self.under_test = Module(self.test_space, self.set_collection)

        self.under_test.previousElement = self.previous_module

        self.under_test.sizeChangeNotifier.addObserver(self.observer)
        self.under_test.addressChangeNotifier.addObserver(self.observer)

    def test_add_single_register(self, this_context):
        expected_name = "r1"

        assert 0 == len(self.under_test["registers"])

        added_register = self.under_test.addRegister(expected_name)

        assert 1 == len(self.under_test["registers"])
        assert (
            expected_name == self.under_test["registers"][expected_name]["name"]
        )
        # The addRegister method returns the created register
        assert added_register == self.under_test["registers"][expected_name]

        assert self.under_test.baseAddress == added_register.startAddress

    def test_add_register_after_multibyte(self, this_context):
        r1 = self.under_test.addRegister("r1")

        r1.addField("f1", (0, 10))

        assert self.under_test.baseAddress == r1.startAddress
        assert 2 == r1.sizeMemoryUnits

        r2 = self.under_test.addRegister("r2")

        expected_r2_address = self.under_test.baseAddress + r1.sizeMemoryUnits

        assert expected_r2_address == r2.startAddress
