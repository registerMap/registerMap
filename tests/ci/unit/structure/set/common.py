#
# Copyright 2023 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest


class IndirectionTestBase:
    """Indirection to prevent running generic tests on the base class."""

    class TestElementSetAdd:
        def test_add_element(self, elements_context):
            under_test = elements_context.set_under_test
            assert len(under_test) == 0

            under_test.add(elements_context.generate_element("e1"))

            assert len(under_test) == 1

    class TestElementSetDiscard:
        @pytest.fixture()
        def discard_context(self, elements_context):
            self.element = elements_context.generate_element("e2")
            elements_context.set_under_test.add(self.element)

        def test_discard_element(self, elements_context, discard_context):
            under_test = elements_context.set_under_test

            assert len(under_test) == 1

            under_test.discard(self.element)

            assert len(under_test) == 0

        def test_discard_absent_element(
            self, elements_context, discard_context
        ):
            under_test = elements_context.set_under_test
            assert len(under_test) == 1

            under_test.discard(elements_context.generate_element("e3"))

            # The element in the set is not the same element being discarded,
            # so the set doesn't change.
            assert len(under_test) == 1

    class TestElementSetSingleElement:
        @pytest.fixture()
        def single_element_context(self, elements_context):
            number_elements = 1
            for this_index in range(0, number_elements):
                this_name = "{0}".format(this_index)

                elements_context.set_under_test.add(
                    elements_context.generate_element(name=this_name)
                )

        def test_find_single_item(
            self, elements_context, single_element_context
        ):
            under_test = elements_context.set_under_test

            assert len(under_test) == 1
            x = list(under_test)

            assert x[0]["name"] == "0"

            # Look for the only item known to be in the set.
            actual_items_found = under_test.find("0")

            assert len(actual_items_found) == 1

            y = list(actual_items_found)
            assert y[0]["name"] == "0"

        def test_items_found_empty_when_element_not_in_set(
            self, elements_context, single_element_context
        ):
            under_test = elements_context.set_under_test

            assert len(under_test) == 1
            x = list(under_test)

            assert x[0]["name"] == "0"

            # Look for an item known to not be in the set.
            actual_items_found = under_test.find("a")

            # Items found is empty.
            assert len(actual_items_found) == 0

    class TestElementSetMultipleElements:
        @pytest.fixture()
        def multiple_elements_context(self, elements_context):
            for i in range(0, 4):
                elements_context.set_under_test.add(
                    elements_context.generate_element(name=repr(i))
                )

            self.number_repeated_elements = 3
            for i in range(0, self.number_repeated_elements):
                elements_context.set_under_test.add(
                    elements_context.generate_element(f"a{i}")
                )
