#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.set.elementSet import ElementSet
from tests.ci.unit.structure.set.common import IndirectionTestBase

from .utility import ElementSetTestBase


class TestConstructElementSet:
    def test_default_constructor(self):
        s = ElementSet()

        assert len(s) == 0


class TestElementSetAdd(IndirectionTestBase.TestElementSetAdd):
    pass


class TestElementSetDiscard(IndirectionTestBase.TestElementSetDiscard):
    pass


class TestElementSetRemove:
    @pytest.fixture()
    def remove_context(self, elements_context: ElementSetTestBase):
        self.element = elements_context.generate_element("e4")
        elements_context.set_under_test.add(self.element)

    def test_remove_element(
        self, elements_context: ElementSetTestBase, remove_context
    ):
        under_test = elements_context.set_under_test
        assert len(under_test) == 1

        under_test.remove(self.element)

        assert len(under_test) == 0

    def test_remove_absent_element_raises(
        self, elements_context: ElementSetTestBase, remove_context
    ):
        under_test = elements_context.set_under_test
        assert len(under_test) == 1

        with pytest.raises(KeyError):
            under_test.remove(elements_context.generate_element("e5"))


class TestElementSetOrder:
    def test_set_order(self, elements_context: ElementSetTestBase):
        under_test = elements_context.set_under_test

        number_elements = 5
        expected_ordered_names = []
        # Add the elements in order
        for x in range(0, number_elements):
            expected_ordered_names.append(repr(x))
            under_test.add(
                elements_context.generate_element(
                    name=expected_ordered_names[x]
                )
            )

        elements = list(under_test)

        actual_ordered_names = [x["name"] for x in elements]

        assert actual_ordered_names == expected_ordered_names


class TestElementSetSingleElement(
    IndirectionTestBase.TestElementSetSingleElement
):
    pass


class TestElementSetMultipleElements(
    IndirectionTestBase.TestElementSetMultipleElements
):
    def test_find_multiple_items(
        self, elements_context, multiple_elements_context
    ):
        """ElementSet doesn't enforce uniqueness of elements."""
        under_test = elements_context.set_under_test

        # Look for the items known to be in the set.
        pre_items_found = under_test.find("a0")
        assert len(pre_items_found) == 1

        # duplicate item
        under_test.add(elements_context.generate_element("a0"))
        actual_items_found = under_test.find("a0")

        assert len(actual_items_found) == 2
