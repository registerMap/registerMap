#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.elements.field import Field
from registermap.structure.elements.register import RegisterInstance
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.set.fieldSet import ConfigurationError, FieldSet


class ElementsContext:
    def __init__(self):
        self.memory = MemoryConfiguration()

        self.set_under_test = FieldSet()

        self.register = RegisterInstance(
            self.memory, setCollection=self.set_under_test
        )


@pytest.fixture()
def elements_context() -> ElementsContext:
    return ElementsContext()


class TestRemoveField:
    def test_nonexistent_field_remove_raises(
        self, elements_context: ElementsContext
    ):
        """
        When: a bit field is removed And: the bit field does not exist in the bit field set Then: an exception is raised
        """
        bf = Field()

        assert len(elements_context.set_under_test) == 0

        with pytest.raises(
            ConfigurationError, match="^Field does not exist in set"
        ):
            elements_context.set_under_test.remove(bf)

    def test_single_non_global_field_remove(
        self, elements_context: ElementsContext
    ):
        """
        When: a bit field is removed And: the bit field exists in the bit field set And: the bit field is defined as non-global Then: the size of the bit field set is reduced by 1
        """

        def check_initial_set():
            nonlocal self, expected_name

            assert len(elements_context.set_under_test) == 1

            bit_fields = elements_context.set_under_test.find(expected_name)
            assert len(bit_fields) == 1

            this_field = bit_fields.pop()

            assert this_field["global"] == False

        expected_name = "test"
        bf = Field(parent=elements_context.register)
        bf["name"] = expected_name

        elements_context.set_under_test.add(bf)

        check_initial_set()

        elements_context.set_under_test.remove(bf)

        assert len(elements_context.set_under_test) == 0

    def test_single_global_field_remove(
        self, elements_context: ElementsContext
    ):
        """
        When: a bit field is removed And: there is a single bit field in the bit field set And: the bit field is defined as non-global Then: the size of the bit field set is reduced by 1
        """

        def check_initial_set():
            nonlocal self, expected_name

            assert len(elements_context.set_under_test) == 1

            bit_fields = elements_context.set_under_test.find(expected_name)
            assert len(bit_fields) == 1

            this_field = bit_fields.pop()

            assert this_field["global"] == True

        expected_name = "test"
        bf = Field()
        bf["name"] = expected_name
        assert bf["global"]

        elements_context.set_under_test.add(bf)

        check_initial_set()

        elements_context.set_under_test.remove(bf)

        assert len(elements_context.set_under_test) == 0
