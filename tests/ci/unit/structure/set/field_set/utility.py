#
# Copyright 2023 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import typing

from registermap.structure.elements.field import Field
from registermap.structure.set.fieldSet import FieldSet
from tests.ci.unit.structure.set.utility import SetTestBase


class FieldSetTestBase(SetTestBase[FieldSet, Field]):
    def __init__(self):
        super().__init__()

    @staticmethod
    def get_set_type() -> typing.Type[FieldSet]:
        return FieldSet

    @staticmethod
    def get_element_type() -> typing.Type[Field]:
        return Field
