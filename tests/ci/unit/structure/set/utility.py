#
# Copyright 2023 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import abc
import typing

T = typing.TypeVar("T", bound="SetTestBase")
U = typing.TypeVar("U")
V = typing.TypeVar("V")


class SetTestBase(typing.Generic[U, V], abc.ABC):
    set_under_test: U

    def __init__(self):
        self.set_under_test = self.get_set_type()()

    @staticmethod
    @abc.abstractmethod
    def get_set_type() -> U:
        raise NotImplementedError()

    @staticmethod
    @abc.abstractmethod
    def get_element_type() -> V:
        raise NotImplementedError()

    def generate_element(self: T, name: str) -> V:
        e = self.get_element_type()()
        e["name"] = name

        return e
