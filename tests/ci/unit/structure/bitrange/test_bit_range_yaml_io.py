#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

import pytest

from registermap.structure.bitrange import BitRange

log = logging.getLogger(__name__)


class TestBitRangeLoadSave:
    @pytest.fixture()
    def this_context(self):
        self.expected_value = [3, 7]
        self.under_test = BitRange(value=self.expected_value)

    def test_load_save(self, this_context):
        encoded_yaml_data = self.under_test.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_data = BitRange.from_yamlData(encoded_yaml_data)

        assert decoded_data.value == self.under_test.value

    def test_none_value_decode(self, this_context):
        yaml_data = {"range": None}
        decoded_data = BitRange.from_yamlData(yaml_data)

        assert decoded_data.value is None
