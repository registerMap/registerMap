#
# Copyright 2016 Russell Smiley
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

import pytest

from registermap.exceptions import IntervalError
from registermap.structure.bitrange import BitRange
from tests.ci.unit.structure.elements.mock_observer import MockObserver

log = logging.getLogger(__name__)


class TestBitRange:
    @pytest.fixture()
    def this_context(self):
        self.under_test = BitRange()
        self.observer = MockObserver()

        self.under_test.sizeChangeNotifier.addObserver(self.observer)

    def test_default_value(self, this_context):
        assert self.under_test == None

    def test_constructed_value(self, this_context):
        expected_value = [3, 6]
        r = BitRange(value=expected_value)
        assert r.value == set(expected_value)

    def test_bad_constructed_value_raises(self, this_context):
        expected_value = "[3, 6]"
        with pytest.raises(
            IntervalError,
            match="^Interval must be a set of one or two positive integers",
        ):
            BitRange(value=expected_value)

    def test_value_assignment(self, this_context):
        expected_value = [0, 8]

        self.under_test.value = expected_value

        assert isinstance(self.under_test, BitRange)
        assert self.under_test == expected_value
        assert self.observer.updated

    def test_max_value(self, this_context):
        expected_value = 7
        self.under_test.value = [2, 4]
        assert self.under_test.maxValue == expected_value

    def test_non_list_raises(self, this_context):
        with pytest.raises(
            IntervalError,
            match="^Interval must be a set of one or two positive integers",
        ):
            self.under_test.value = "5"

    def test_list_non_int_raises(self, this_context):
        with pytest.raises(
            IntervalError,
            match="^Interval must be a set of one or two positive integers",
        ):
            self.under_test.value = [5, "6"]

    def test_negative_values_raises(self, this_context):
        with pytest.raises(
            IntervalError,
            match="^Interval must be a set of one or two positive integers",
        ):
            self.under_test.value = [5, -7]

    def test_wrong_length_raises(self, this_context):
        with pytest.raises(
            IntervalError,
            match="^Interval must be a set of one or two positive integers",
        ):
            self.under_test.value = [5, 8, 6]


class TestNumberBits:
    @pytest.fixture()
    def this_context(self):
        self.under_test = BitRange()

    def test_calculate_number_bits(self, this_context):
        expected_value = 5
        self.under_test.value = [2, 6]
        assert self.under_test.numberBits == expected_value

    def test_number_bits_none_value_is_zero(self, this_context):
        assert self.under_test.numberBits == 0


class TestEqualOperator:
    @pytest.fixture()
    def this_context(self):
        self.expected_value = [3, 7]
        self.under_test = BitRange(value=self.expected_value)

    def test_list(self, this_context):
        assert self.under_test == self.expected_value

    def test_none(self, this_context):
        # It is necessary to use == operator here because 'is' operator cannot be overridden in Python.
        assert not self.under_test == None

    def test_bit_range(self, this_context):
        assert self.under_test == BitRange(value=self.expected_value)
        assert not self.under_test == BitRange(value=[0, 4])


class TestNotEqualOperator:
    @pytest.fixture()
    def this_context(self):
        self.expected_value = [3, 7]
        self.under_test = BitRange(value=self.expected_value)

    def test_list(self, this_context):
        assert self.under_test != [0, 4]

    def test_none(self, this_context):
        # It is necessary to use != operator here because 'is' operator cannot
        # be overridden in Python.
        assert self.under_test != None

    def test_bit_range(self, this_context):
        assert not self.under_test != BitRange(value=self.expected_value)
        assert self.under_test != BitRange(value=[0, 4])
