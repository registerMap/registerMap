#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.bitmap.bitmap import (
    BitMap,
    BitRange,
    ConfigurationError,
)

from .mocks import MockBitStore


class TestAssignOverlappingSourceIntervals:
    @pytest.fixture()
    def this_context(self):
        self.source_size = 16
        self.source_id = "testSource"
        self.mock_source = MockBitStore(self.source_id, self.source_size)
        self.source_bit_map = BitMap(self.mock_source)
        self.mock_source.bitMap = self.source_bit_map

        self.destination_size = 4
        self.destination_id = "testDestination"
        self.mock_destination = MockBitStore(
            self.destination_id, self.destination_size
        )
        self.destination_bit_map = BitMap(self.mock_destination)
        self.mock_destination.bitMap = self.destination_bit_map

    def test_overlapped_source_interval_raises(self, this_context):
        field_ranges = [
            BitRange((0, 1)),
            BitRange((2, 3)),
        ]
        register_bit_offset = 4
        register_ranges = [
            BitRange(
                (
                    register_bit_offset,
                    (register_bit_offset + self.destination_size - 1 - 2),
                )
            ),
            BitRange(
                (
                    (register_bit_offset + self.destination_size - 1 - 2),
                    (register_bit_offset + self.destination_size - 1),
                )
            ),
        ]

        self.source_bit_map.mapBits(
            register_ranges[0], field_ranges[0], self.mock_destination
        )

        assert len(self.source_bit_map.intervalMap) == 1

        with pytest.raises(
            ConfigurationError,
            match="^Specifed source interval overlaps existing source intervals",
        ):
            self.source_bit_map.mapBits(
                register_ranges[1], field_ranges[1], self.mock_destination
            )

    def test_identical_source_interval_writes_new_mapping(self, this_context):
        # Equal source interval writes the new mapping.
        field_ranges = [
            BitRange((0, 1)),
            BitRange((2, 3)),
        ]
        register_bit_offset = 4
        register_range = BitRange(
            (
                register_bit_offset,
                (register_bit_offset + self.destination_size - 1 - 2),
            )
        )

        self.source_bit_map.mapBits(
            register_range, field_ranges[0], self.mock_destination
        )

        assert len(self.source_bit_map.intervalMap) == 1

        self.source_bit_map.mapBits(
            register_range, field_ranges[1], self.mock_destination
        )

        assert len(self.source_bit_map.intervalMap) == 1
        actual_intervals = self.source_bit_map.intervalMap

        assert len(actual_intervals) == 1
        actual_source_range, destination_data = actual_intervals.popitem()

        assert actual_source_range == register_range

        self.check_destination_data(
            destination_data, self.mock_destination, field_ranges[1]
        )

    def check_destination_data(
        self, actual_data, expected_object, expected_interval
    ):
        assert actual_data["destination"] == expected_object
        assert actual_data["interval"] == expected_interval

    def test_adding_different_intervals_increases_size(self, this_context):
        field_ranges = [
            BitRange((0, 1)),
            BitRange((2, 3)),
        ]
        register_bit_offset = 4
        register_ranges = [
            BitRange(
                (
                    register_bit_offset,
                    (register_bit_offset + self.destination_size - 1 - 2),
                )
            ),
            BitRange(
                (
                    (register_bit_offset + self.destination_size - 1 - 1),
                    (register_bit_offset + self.destination_size - 1),
                )
            ),
        ]

        self.source_bit_map.mapBits(
            register_ranges[0], field_ranges[0], self.mock_destination
        )
        assert len(self.source_bit_map.intervalMap) == 1

        self.source_bit_map.mapBits(
            register_ranges[1], field_ranges[1], self.mock_destination
        )

        actual_intervals = self.source_bit_map.intervalMap
        assert len(actual_intervals) == 2

        destination_data = actual_intervals[register_ranges[0]]
        self.check_destination_data(
            destination_data, self.mock_destination, field_ranges[0]
        )

        destination_data = actual_intervals[register_ranges[1]]
        self.check_destination_data(
            destination_data, self.mock_destination, field_ranges[1]
        )
