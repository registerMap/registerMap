#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

import pytest

from registermap.structure.bitmap import BitMap
from registermap.structure.bitrange import BitRange
from registermap.structure.elements.field import Field

from .mocks import MockBitStore

log = logging.getLogger(__name__)


class TestEncodeYamlBitMap:
    @pytest.fixture()
    def this_context(self):
        self.mock_source = MockBitStore("module.register", 10)
        self.under_test = BitMap(self.mock_source)

        self.mock_source.bitMap = self.under_test

    def test_encode_empty_bit_map(self, this_context):
        # An empty bitmap encodes only the source info

        encoded_yaml_data = self.under_test.to_yamlData()

        assert encoded_yaml_data["bitmap"] == list()


class TestEncodeYamlBitMapIntervals:
    """
    Demonstrate YAML encoding BitMap intervals.
    """

    @pytest.fixture()
    def this_context(self, mocker):
        def construct_mock_destination():
            nonlocal mocker, self

            self.destination_mock_bitmap = mocker.create_autospec(BitMap)

            self.mock_destination = mocker.create_autospec(Field)

            idp = mocker.PropertyMock(return_value="module.register.field")
            type(self.mock_destination).canonicalId = idp

            idbm = mocker.PropertyMock(
                return_value=self.destination_mock_bitmap
            )
            type(self.mock_destination).bitMap = idbm

        self.mock_source = MockBitStore("module.register", 10)
        self.bit_map_under_test = BitMap(self.mock_source)

        self.mock_source.bitMap = self.bit_map_under_test

        construct_mock_destination()

    def test_encode_mapping(self, this_context, mocker):
        mocker.patch.object(
            self.bit_map_under_test, "_BitMap__validateBitRange"
        )
        mocker.patch.object(
            self.bit_map_under_test, "_BitMap__validateSourceOverlaps"
        )
        self.bit_map_under_test.mapBits(
            BitRange([4, 7]), BitRange((0, 3)), self.mock_destination
        )

        encoded_yaml_data = self.bit_map_under_test.to_yamlData()

        assert len(encoded_yaml_data["bitmap"]) == 1
        assert encoded_yaml_data["bitmap"][0]["source"] == "[4:7]"
        assert encoded_yaml_data["bitmap"][0]["destination"] == "[0:3]"
        assert (
            encoded_yaml_data["bitmap"][0]["destinationId"]
            == self.mock_destination.canonicalId
        )
