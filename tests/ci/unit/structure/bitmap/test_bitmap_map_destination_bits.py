#
# Copyright 2017 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

import pytest

from registermap.structure.bitmap.bitmap import (
    BitMap,
    BitRange,
    DestinationIntervalError,
)

from .mocks import MockBitStore


class TestDestinationRangePersists:
    @pytest.fixture()
    def this_context(self):
        self.source_size = 16
        self.source_id = "testSource"
        self.mock_source = MockBitStore(self.source_id, self.source_size)
        self.source_bit_map = BitMap(self.mock_source)
        self.mock_source.bitMap = self.source_bit_map

        self.destination_size = 4
        self.destination_id = "testDestination"
        self.mock_destination = MockBitStore(
            self.destination_id, self.destination_size
        )
        self.destination_bit_map = BitMap(self.mock_destination)
        self.mock_destination.bitMap = self.destination_bit_map

        self.destination_ranges = [
            BitRange((0, 2)),
        ]
        self.source_bit_offset = 4
        self.source_ranges = [
            BitRange((self.source_bit_offset, (self.source_bit_offset + 2))),
        ]

        assert self.mock_source not in self.destination_bit_map.destinations
        assert self.source_bit_map.source == self.mock_source
        assert self.source_bit_map.destinations == set()

        assert self.mock_destination not in self.source_bit_map.destinations
        assert self.destination_bit_map.source == self.mock_destination
        assert self.destination_bit_map.destinations == set()

        self.source_bit_map.mapBits(
            self.source_ranges[0],
            self.destination_ranges[0],
            self.mock_destination,
        )

    def test_destination_added_to_source(self, this_context):
        """
        Creating a mapping stores the destination in the source BitMap.
        """

        assert self.mock_destination in self.source_bit_map.destinations

        actual_interval_map = self.source_bit_map.intervalMap
        assert len(actual_interval_map) == 1

        (
            actual_source_interval,
            actual_destination,
        ) = actual_interval_map.popitem()
        assert actual_source_interval == self.source_ranges[0]
        assert actual_destination["destination"] == self.mock_destination
        assert actual_destination["interval"] == self.destination_ranges[0]

    def test_source_added_to_destination_as_destination(self, this_context):
        """
        Creating a mapping stores the source in the destination BitMap as a destination.
        """
        assert self.mock_source in self.destination_bit_map.destinations

        actual_interval_map = self.destination_bit_map.intervalMap
        assert len(actual_interval_map) == 1

        (
            actual_source_interval,
            actual_destination,
        ) = actual_interval_map.popitem()
        assert actual_source_interval == self.destination_ranges[0]
        assert actual_destination["destination"] == self.mock_source
        assert actual_destination["interval"] == self.source_ranges[0]

    def test_destination_added_to_destination_as_source(self, this_context):
        """
        Creating a mapping stores the destination in the destination BitMap as the source of that BitMap.
        """

        assert self.destination_bit_map.source == self.mock_destination


class TestAssignOverlappingDestinationIntervals:
    @pytest.fixture()
    def this_context(self):
        self.source_size = 16
        self.source_id = "testSource"
        self.mock_source = MockBitStore(self.source_id, self.source_size)
        self.source_bit_map = BitMap(self.mock_source)
        self.mock_source.bitMap = self.source_bit_map

        self.destination_size = 4
        self.destination_id = "testDestination"
        self.mock_destination = MockBitStore(
            self.destination_id, self.destination_size
        )
        self.destination_bit_map = BitMap(self.mock_destination)
        self.mock_destination.bitMap = self.destination_bit_map

    def test_overlapped_destination_interval_raises(self, this_context):
        destination_ranges = [
            BitRange((0, 2)),
            BitRange((2, 3)),
        ]
        source_bit_offset = 4
        source_ranges = [
            BitRange((source_bit_offset, (source_bit_offset + 2))),
            BitRange(((source_bit_offset + 3), (source_bit_offset + 4))),
        ]

        self.source_bit_map.mapBits(
            source_ranges[0], destination_ranges[0], self.mock_destination
        )

        assert len(self.source_bit_map.intervalMap) == 1

        with pytest.raises(
            DestinationIntervalError,
            match="^Specifed destination interval overlaps existing destination intervals",
        ):
            self.source_bit_map.mapBits(
                source_ranges[1], destination_ranges[1], self.mock_destination
            )
