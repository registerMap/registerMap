#
# Copyright 2016 Russell Smiley
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Test ``AddressableMemoryElement`` methods and properties.
"""

import logging

import pytest

from registermap.exceptions import ConfigurationError
from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.memory.element import (
    AddressableMemoryElement,
    BitsMemoryElement,
)

log = logging.getLogger(__name__)


class TestDefaultConstructor:
    """Test ``AddressableMemoryElement`` default constructor."""

    @pytest.fixture()
    def this_context(self):
        self.memory = MemoryConfiguration()

        assert 8 == self.memory.memoryUnitBits

    def test_default_arguments(self, this_context):
        under_test = AddressableMemoryElement(self.memory)

        assert under_test.startAddress is None
        assert under_test.endAddress is None
        assert 1 == under_test.sizeMemoryUnits

    def test_assign_start_address_with_none_size_end(self, this_context):
        under_test = AddressableMemoryElement(self.memory)

        under_test.startAddress = 2
        under_test.sizeMemoryUnits = None

        assert 2 == under_test.startAddress
        assert under_test.endAddress is None
        assert under_test.sizeMemoryUnits is None

    def test_assign_start_address_with_default_size(self, this_context):
        under_test = AddressableMemoryElement(self.memory)

        under_test.startAddress = 2

        assert 2 == under_test.startAddress
        assert 2 == under_test.endAddress

    def test_none_size_end_then_assign_start_address(self, this_context):
        under_test = AddressableMemoryElement(self.memory)

        under_test.sizeMemoryUnits = None
        under_test.startAddress = 2

        assert 2 == under_test.startAddress
        assert under_test.endAddress is None
        assert under_test.sizeMemoryUnits is None

    def test_valid_start_address_argument(self, this_context):
        expected_value = 0x10
        under_test = AddressableMemoryElement(
            self.memory, startAddress=expected_value
        )

        assert expected_value == under_test.startAddress
        assert expected_value == under_test.endAddress
        assert 1 == under_test.sizeMemoryUnits

    def test_valid_start_end_address_arguments(self, this_context):
        expected_start_address = 0x10
        expected_end_address = 0x15
        under_test = AddressableMemoryElement(
            self.memory,
            startAddress=expected_start_address,
            endAddress=expected_end_address,
        )

        assert expected_start_address == under_test.startAddress
        assert expected_end_address == under_test.endAddress
        assert (
            expected_end_address - expected_start_address + 1
        ) == under_test.sizeMemoryUnits

    def test_valid_start_address_size_arguments(self, this_context):
        expected_start_address = 0x10
        expected_size = 5
        under_test = AddressableMemoryElement(
            self.memory,
            startAddress=expected_start_address,
            sizeMemoryUnits=expected_size,
        )

        assert expected_start_address == under_test.startAddress
        assert expected_size == under_test.sizeMemoryUnits
        assert (
            expected_start_address + expected_size - 1
        ) == under_test.endAddress

    def test_specify_both_end_address_and_size_raises(self, this_context):
        with pytest.raises(
            ConfigurationError,
            match="^Cannot specify both endAddress and sizeMemoryUnits",
        ):
            AddressableMemoryElement(
                self.memory, endAddress=4, sizeMemoryUnits=6
            )

    def test_size_object_non_default_size_none_start(self, this_context):
        expected_size_bits = 16
        size = BitsMemoryElement(self.memory)
        size.sizeBits = expected_size_bits

        element_under_test = AddressableMemoryElement(
            self.memory, sizeObject=size
        )

        assert (
            int(expected_size_bits / self.memory.memoryUnitBits)
            == element_under_test.sizeMemoryUnits
        )

    def test_size_object_non_default_size_numeric_start(self, this_context):
        expected_size_bits = 16
        size = BitsMemoryElement(self.memory)
        size.sizeBits = expected_size_bits

        element_under_test = AddressableMemoryElement(
            self.memory, sizeObject=size, startAddress=0x10
        )

        assert (
            int(expected_size_bits / self.memory.memoryUnitBits)
            == element_under_test.sizeMemoryUnits
        )


class TestMemoryElementOffset:
    @pytest.fixture()
    def this_context(self):
        self.memory = MemoryConfiguration()
        self.memory.baseAddress = 0x2010

        self.under_test = AddressableMemoryElement(self.memory)

    def test_default_value(self, this_context):
        assert self.under_test.offset is None

    def test_offset(self, this_context):
        self.under_test.startAddress = 0x21E4

        expected_value = self.under_test.startAddress - self.memory.baseAddress

        assert expected_value == self.under_test.offset


class TestMemoryElementStartAddress:
    """Test ``MemoryElement`` start address behaviour"""

    @pytest.fixture()
    def this_context(self):
        self.memory = MemoryConfiguration()
        self.under_test = AddressableMemoryElement(self.memory)

    def test_default_value(self, this_context):
        assert self.under_test.startAddress is None

    def test_assign_value_no_size(self, this_context):
        expected_value = 0x10
        assert self.under_test.startAddress != expected_value

        self.under_test.startAddress = expected_value

        assert expected_value == self.under_test.startAddress
        assert self.under_test.endAddress == self.under_test.startAddress

    def test_assign_value_size_defined(self, this_context):
        """
        Assigning a start address when the size is already defined, correctly defines the end address.
        """
        self.under_test.sizeMemoryUnits = 5

        expected_start_address = 0x10
        expected_end_address = 0x14

        # Test that the element doesn't already have these values.
        assert expected_start_address != self.under_test.startAddress
        assert expected_end_address != self.under_test.endAddress

        # Assign the start address
        self.under_test.startAddress = expected_start_address

        # Test that start and end addresses are correct.
        assert expected_start_address == self.under_test.startAddress
        assert expected_end_address == self.under_test.endAddress

    def test_assign_value_with_end_address(self, this_context):
        initial_start_address = 0x10
        assert initial_start_address != self.under_test.startAddress

        self.under_test.startAddress = initial_start_address
        self.under_test.sizeMemoryUnits = 5

        expected_start_address = 0x10
        self.under_test.startAddress = expected_start_address

        log.debug(
            "Size after start address change: "
            + repr(self.under_test.sizeMemoryUnits)
        )

        assert expected_start_address == self.under_test.startAddress
        assert (
            expected_start_address + self.under_test.sizeMemoryUnits - 1
        ) == self.under_test.endAddress

    def test_unassigned_start_address_raises(self, this_context):
        under_test = AddressableMemoryElement(self.memory)
        with pytest.raises(
            ConfigurationError,
            match="^Must define start address before attempting to define end address",
        ):
            under_test.endAddress = 0x5

    def test_assign_none(self, this_context):
        expected_value = 0x10
        self.under_test.startAddress = expected_value
        assert expected_value == self.under_test.startAddress

        self.under_test.startAddress = None

        assert self.under_test.startAddress is None
        assert self.under_test.endAddress is None


class TestMemoryElementEndAddress:
    @pytest.fixture()
    def this_context(self):
        self.memory = MemoryConfiguration()
        self.under_test = AddressableMemoryElement(self.memory)

    def test_default_value(self, this_context):
        assert self.under_test.endAddress is None

    def test_assign_value(self, this_context):
        # Assign the start address
        self.under_test.startAddress = 0x10
        expected_end_address = 0x20
        assert expected_end_address != self.under_test.endAddress

        # Assign the end address
        self.under_test.endAddress = expected_end_address

        # Expect the size to be modified as a result of the end address assignment.
        assert expected_end_address == self.under_test.endAddress
        assert (
            expected_end_address - self.under_test.startAddress + 1
        ) == self.under_test.sizeMemoryUnits


class TestMemoryElementSize:
    """Test ``AddressableMemoryElement`` size property."""

    @pytest.fixture()
    def this_context(self):
        self.memory = MemoryConfiguration()
        self.under_test = AddressableMemoryElement(self.memory)

    def test_default_value(self, this_context):
        assert 1 == self.under_test.sizeMemoryUnits

    def test_value_after_initial_start_address_assign(self, this_context):
        self.under_test.startAddress = 0x10

        assert 1 == self.under_test.sizeMemoryUnits

    def test_assign_value(self, this_context):
        """Assigning a size to a memory element with a pre-defined start address
        assigns the size and re-defines the end address of the element.
        """
        self.under_test.startAddress = 0x10
        expected_size = 5
        assert expected_size != self.under_test.sizeMemoryUnits

        self.under_test.sizeMemoryUnits = expected_size

        assert expected_size == self.under_test.sizeMemoryUnits
        assert (
            self.under_test.startAddress + self.under_test.sizeMemoryUnits - 1
        ) == self.under_test.endAddress

    def test_premature_assignment_no_raise(self, this_context):
        assert self.under_test.startAddress is None
        assert self.under_test.endAddress is None

        expected_value = 5
        self.under_test.sizeMemoryUnits = expected_value

        assert expected_value == self.under_test.sizeMemoryUnits
