#
# Copyright 2018 Russell Smiley
#
# This file is part of registerMap.
#
# registerMap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# registerMap is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with registerMap.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Test ``BitsMemoryELement``.
"""

import pytest

from registermap.structure.memory.configuration import MemoryConfiguration
from registermap.structure.memory.element import BitsMemoryElement


class TestBitsMemoryElementConstructor:
    """Test ``BitsMemoryElement`` constructor."""

    @pytest.fixture()
    def this_context(self):
        self.memory = MemoryConfiguration()

    def test_default_arguments(self, this_context):
        under_test = BitsMemoryElement()

        assert under_test.sizeBits is None

    def test_construct_size(self, this_context):
        expected_size = 10
        under_test = BitsMemoryElement(expected_size)

        assert expected_size == under_test.sizeBits


class TestBitsMemoryElementSizeProperties:
    """
    Test ``BitsMemoryElement`` size properties.
    """

    @pytest.fixture()
    def this_context(self, mocker):
        self.memory = MemoryConfiguration()
        self.mock_observer = mocker.MagicMock()

        self.under_test = BitsMemoryElement()
        self.under_test.addObserver(self.mock_observer)

    def test_assign_size_observer_notified(self, this_context):
        assert self.under_test.sizeBits is None
        expected_size = 1
        self.under_test.sizeBits = expected_size

        assert expected_size == self.under_test.sizeBits
        self.mock_observer.update.assert_called_once()

    def test_assign_size_observer_not_notified(self, this_context):
        assert self.under_test.sizeBits is None
        expected_size = 1
        self.under_test.sizeBitsNoNotify = expected_size

        assert expected_size == self.under_test.sizeBits
        self.mock_observer.update.assert_not_called()
