"""
Unit tests for MemorySpace.
"""
#
# Copyright 2016 Russell Smiley
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

import pytest

from registermap.structure.memory.configuration import (
    ConfigurationError,
    MemoryConfiguration,
)
from tests.ci.unit.structure.elements.mock_observer import MockObserver

log = logging.getLogger(__name__)


class TestBaseAddress:
    @pytest.fixture()
    def this_context(self):
        self.this_space = MemoryConfiguration()

        self.observer = MockObserver()
        self.this_space.addressChangeNotifier.addObserver(self.observer)
        self.this_space.sizeChangeNotifier.addObserver(self.observer)

    def test_default_base_address(self, this_context):
        expected_base_address = 0
        actual_base_address = self.this_space.baseAddress

        assert isinstance(actual_base_address, int)
        assert actual_base_address == expected_base_address
        assert self.observer.update_count == 0

    def test_set_base_address_good_value(self, this_context):
        expected_base_address = 25

        # Don't test with the default value
        assert expected_base_address != self.this_space.baseAddress

        self.this_space.baseAddress = expected_base_address
        actual_base_address = self.this_space.baseAddress

        assert actual_base_address == expected_base_address
        assert self.observer.update_count == 1
        assert self.observer.arguments == "baseAddress"

    def test_non_int_raises(self, this_context):
        expected_base_address = "25"

        with pytest.raises(
            ConfigurationError,
            match="^Base address must be specified as non-negative integer",
        ):
            self.this_space.baseAddress = expected_base_address

    def test_negative_raises(self, this_context):
        with pytest.raises(
            ConfigurationError,
            match="^Base address must be specified as non-negative integer",
        ):
            self.this_space.baseAddress = -1

    def test_base_address_greater_than_memory_address_bits_raises(
        self, this_context
    ):
        self.this_space.addressBits = 4

        expected_base_address = 20

        with pytest.raises(
            ConfigurationError,
            match="^Base address must be less than maximum addressable memory",
        ):
            self.this_space.baseAddress = expected_base_address


class TestMaximumAddressableMemory:
    @pytest.fixture()
    def this_context(self):
        self.this_space = MemoryConfiguration()

    def test_default_maximum_addressable_memory(self, this_context):
        expected_maximum_addressable_memory = pow(
            2, self.this_space.addressBits
        )
        actual_maximum_addressable_memory = self.this_space.maximumMemoryAddress

        assert (
            actual_maximum_addressable_memory
            == expected_maximum_addressable_memory
        )

    def test_set_number_of_address_bits(self, this_context):
        number_bits = 24
        assert self.this_space.addressBits != number_bits

        self.this_space.addressBits = number_bits

        assert self.this_space.addressBits == number_bits
        assert self.this_space.maximumMemoryAddress, pow(2 == number_bits)


class TestMemoryAddress:
    @pytest.fixture()
    def this_context(self):
        self.this_space = MemoryConfiguration()

        self.observer = MockObserver()
        self.this_space.addressChangeNotifier.addObserver(self.observer)

    def test_default_memory_address_bits(self, this_context):
        expected_memory_address_bits = 32
        actual_memory_address_bits = self.this_space.addressBits

        assert isinstance(actual_memory_address_bits, int)
        assert actual_memory_address_bits == expected_memory_address_bits

    def test_set_good_value(self, this_context):
        expected_memory_address_bits = 20

        assert expected_memory_address_bits != self.this_space.addressBits

        self.this_space.addressBits = expected_memory_address_bits
        actual_memory_address_bits = self.this_space.addressBits

        assert actual_memory_address_bits == expected_memory_address_bits

    def test_non_int_raises(self, this_context):
        with pytest.raises(
            ConfigurationError,
            match="^Memory address bits must be specified as positive non-zero integer",
        ):
            self.this_space.addressBits = "20"

    def test_zero_negative_raises(self, this_context):
        with pytest.raises(
            ConfigurationError,
            match="^Memory address bits must be specified as positive non-zero integer",
        ):
            self.this_space.addressBits = 0

    def test_set_memory_address_bits_less_than_base_address_raises(
        self, this_context
    ):
        self.this_space.baseAddress = 20
        expected_memory_address_bits = 4

        with pytest.raises(
            ConfigurationError,
            match="^Addressable memory must be greater than the base address",
        ):
            self.this_space.addressBits = expected_memory_address_bits

    def test_none_page_size_no_notify_on_change(self, this_context):
        assert self.this_space.pageSize is None
        assert self.observer.update_count == 0

        self.this_space.addressBits = 16

        assert self.observer.update_count == 0

    def test_assigned_page_size_notify_on_change(self, this_context):
        self.this_space.pageSize = 0x80
        # One notification for the page size change
        assert self.observer.update_count == 1
        assert self.observer.arguments == "pageSize"

        self.this_space.addressBits = 16

        assert self.observer.update_count == 2
        assert self.observer.arguments == "addressBits"


class TestMemoryUnit:
    @pytest.fixture()
    def this_context(self):
        self.this_space = MemoryConfiguration()

        self.observer = MockObserver()
        self.this_space.addressChangeNotifier.addObserver(self.observer)

    def test_default_memory_unit_bits(self, this_context):
        expected_memory_unit_bits = 8
        actual_memory_unit_bits = self.this_space.memoryUnitBits

        assert isinstance(actual_memory_unit_bits, int)
        assert actual_memory_unit_bits == expected_memory_unit_bits

    def test_direct_assignment(self, this_context):
        expected_value = 16
        assert expected_value != self.this_space.memoryUnitBits

        self.this_space.memoryUnitBits = expected_value

        assert self.this_space.memoryUnitBits == expected_value

    def test_non_int_raises(self, this_context):
        with pytest.raises(
            ConfigurationError,
            match="^Memory unit bits must be specified as positive non-zero integer",
        ):
            self.this_space.memoryUnitBits = "5"

    def test_zero_raises(self, this_context):
        with pytest.raises(
            ConfigurationError,
            match="^Memory unit bits must be specified as positive non-zero integer",
        ):
            self.this_space.memoryUnitBits = 0

    def test_negative_raises(self, this_context):
        with pytest.raises(
            ConfigurationError,
            match="^Memory unit bits must be specified as positive non-zero integer",
        ):
            self.this_space.memoryUnitBits = -1

    def test_none_page_size_no_notify_on_change(self, this_context):
        assert self.this_space.pageSize is None
        assert self.observer.update_count == 0

        self.this_space.memoryUnitBits = 16

        assert self.observer.update_count == 0

    def test_assigned_page_size_notify_on_change(self, this_context):
        self.this_space.pageSize = 0x80
        # One notification for the page size change
        assert self.observer.update_count == 1
        assert self.observer.arguments == "pageSize"

        self.this_space.memoryUnitBits = 16

        assert self.observer.update_count == 2
        assert self.observer.arguments == "memoryUnitBits"


class TestPageSize:
    @pytest.fixture()
    def this_context(self):
        self.this_space = MemoryConfiguration()

        self.observer = MockObserver()
        self.this_space.addressChangeNotifier.addObserver(self.observer)

    def test_default_page_size(self, this_context):
        actual_page_size = self.this_space.pageSize

        assert actual_page_size is None

    def test_page_size_assignment(self, this_context):
        expected_page_size = 128

        assert expected_page_size != self.this_space.pageSize

        self.this_space.pageSize = expected_page_size
        actual_page_size = self.this_space.pageSize

        assert actual_page_size == expected_page_size

    def test_non_int_raises(self, this_context):
        with pytest.raises(
            ConfigurationError, match="^Page size must be specified as integer"
        ):
            self.this_space.pageSize = "5"

    def test_none_okay(self, this_context):
        self.this_space.pageSize = None

        assert self.this_space.pageSize is None

    def test_page_size_less_than_memory_address_raises(self, this_context):
        expected_page_size = 3

        assert self.this_space.memoryUnitBits == 8
        assert self.this_space.addressBits == 32

        with pytest.raises(ConfigurationError, match="^Bad page size"):
            self.this_space.pageSize = expected_page_size

    def test_change_page_size_notifies_observer(self, this_context):
        assert self.observer.update_count == 0

        self.this_space.pageSize = 0x80

        assert self.observer.update_count == 1
        assert self.observer.arguments == "pageSize"


class TestPageRegisters:
    @pytest.fixture()
    def this_context(self):
        self.this_space = MemoryConfiguration()

    def test_page_registers(self, this_context):
        self.this_space.addressBits = 32
        self.this_space.memoryUnitBits = 8

        expected_number_page_registers = int(
            self.this_space.addressBits / self.this_space.memoryUnitBits
        )

        self.this_space.pageSize = 0x80
        expected_page_register_test = [True, True, True, True, False, False]
        actual_page_register_test = list()
        for offset in range(expected_number_page_registers, 0, -1):
            thisAddress = (self.this_space.pageSize * 3) - offset

            actual_page_register_test.append(
                self.this_space.isPageRegister(thisAddress)
            )

        actual_page_register_test.append(
            self.this_space.isPageRegister(5 * self.this_space.pageSize)
        )
        actual_page_register_test.append(
            self.this_space.isPageRegister((self.this_space.pageSize * 4) - 5)
        )

        assert actual_page_register_test == expected_page_register_test

    def test_page_base_address(self, this_context):
        self.this_space.pageSize = 0x80

        expected_page_base_addresses = [
            self.this_space.pageSize * 2,
            self.this_space.pageSize * 4,
            self.this_space.pageSize * 7,
        ]

        test_addresses = [
            expected_page_base_addresses[0],
            expected_page_base_addresses[1] + 0x7F,
            expected_page_base_addresses[2] + 3,
        ]

        for this_address, page_base_address in zip(
            test_addresses, expected_page_base_addresses
        ):
            actual_page_base_address = self.this_space.pageBaseAddress(
                this_address
            )

            assert actual_page_base_address == page_base_address


class TestCalculatePageRegisterImpact:
    @pytest.fixture()
    def this_context(self):
        self.this_space = MemoryConfiguration()
        self.this_space.pageSize = 0x80

    def test_no_shift_non_page_register(self, this_context):
        assert self.this_space.addressBits == 32
        assert self.this_space.memoryUnitBits == 8

        expected_address = 0x27B
        actual_address = self.this_space.calculatePageRegisterImpact(
            expected_address
        )

        assert actual_address == expected_address

    def test_shift_on_page_register(self, this_context):
        assert self.this_space.addressBits == 32
        assert self.this_space.memoryUnitBits == 8

        expected_address = 0x400
        input_address = 0x3FC
        actual_address = self.this_space.calculatePageRegisterImpact(
            input_address
        )

        assert actual_address == expected_address


class TestYamlLoadSave:
    @pytest.fixture()
    def this_context(self):
        self.this_space = MemoryConfiguration()

    def test_encode_decode(self, this_context):
        encoded_yaml_data = self.this_space.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_memory_space = MemoryConfiguration.from_yamlData(
            encoded_yaml_data
        )

        assert decoded_memory_space.addressBits == self.this_space.addressBits
        assert decoded_memory_space.baseAddress == self.this_space.baseAddress
        assert (
            decoded_memory_space.memoryUnitBits
            == self.this_space.memoryUnitBits
        )
        assert decoded_memory_space.pageSize == self.this_space.pageSize

    def test_non_default_values(self, this_context):
        self.this_space.baseAddress = 0x10
        self.this_space.addressBits = 24
        self.this_space.memoryUnitBits = 16
        self.this_space.pageSize = 128

        encoded_yaml_data = self.this_space.to_yamlData()
        log.debug("Encoded yaml data: " + repr(encoded_yaml_data))
        decoded_memory_space = MemoryConfiguration.from_yamlData(
            encoded_yaml_data
        )

        assert decoded_memory_space.addressBits == self.this_space.addressBits
        assert decoded_memory_space.baseAddress == self.this_space.baseAddress
        assert (
            decoded_memory_space.memoryUnitBits
            == self.this_space.memoryUnitBits
        )
        assert decoded_memory_space.pageSize == self.this_space.pageSize

    def test_optional_address_bits(self, this_context):
        yaml_data = {
            "memorySpace": {
                "baseAddress": 0x10,
                "memoryUnitBits": 16,
                "pageSizeMemoryUnits": 128,
            }
        }
        decoded_memory_space = MemoryConfiguration.from_yamlData(yaml_data)

        assert decoded_memory_space.addressBits == 32
        assert decoded_memory_space.baseAddress == 0x10
        assert decoded_memory_space.memoryUnitBits == 16
        assert decoded_memory_space.pageSize == 128

    def test_optional_base_address(self, this_context):
        yaml_data = {
            "memorySpace": {
                "addressBits": 24,
                "memoryUnitBits": 16,
                "pageSizeMemoryUnits": 128,
            }
        }
        decoded_memory_space = MemoryConfiguration.from_yamlData(yaml_data)

        assert decoded_memory_space.addressBits == 24
        assert decoded_memory_space.baseAddress == 0
        assert decoded_memory_space.memoryUnitBits == 16
        assert decoded_memory_space.pageSize == 128

    def test_optional_memory_unit_bits(self, this_context):
        yaml_data = {
            "memorySpace": {
                "addressBits": 24,
                "baseAddress": 0x10,
                "pageSizeMemoryUnits": 128,
            }
        }
        decoded_memory_space = MemoryConfiguration.from_yamlData(yaml_data)

        assert decoded_memory_space.addressBits == 24
        assert decoded_memory_space.baseAddress == 0x10
        assert decoded_memory_space.memoryUnitBits == 8
        assert decoded_memory_space.pageSize == 128

    def test_optional_page_size_memory_units(self, this_context):
        yaml_data = {
            "memorySpace": {
                "addressBits": 24,
                "memoryUnitBits": 16,
                "baseAddress": 0x10,
            }
        }
        decoded_memory_space = MemoryConfiguration.from_yamlData(yaml_data)

        assert decoded_memory_space.addressBits == 24
        assert decoded_memory_space.baseAddress == 0x10
        assert decoded_memory_space.memoryUnitBits == 16
        assert decoded_memory_space.pageSize is None
