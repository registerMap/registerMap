/*
 *
 {%- if licenseText is not none %}
 {%- for line in licenseText %}
 * {{ line }}
 {%- endfor %}
 {%- endif %}
 *
 */

#ifndef {{ registermapName|upper }}_EXTERN_H
#define {{ registermapName|upper }}_EXTERN_H

#ifdef __cplusplus

#define {{ registermapName|upper }}_OPEN_EXTERN_C extern "C" {
#define {{ registermapName|upper }}_CLOSE_EXTERN_C }

#else

/* Empty macro to disable extern C */
#define {{ registermapName|upper }}_OPEN_EXTERN_C
#define {{ registermapName|upper }}_CLOSE_EXTERN_C

#endif

#endif
