/*
 *
 * {{ registermap.name }}
 *
 {%- if licenseText is not none %}
 {%- for line in licenseText %}
 * {{ line }}
 {%- endfor %}
 {%- endif %}
 *
 */

#ifndef {{ registermap.name|upper }}_H
#define {{ registermap.name|upper }}_H

#include "{{ prefixPath }}/macro/extern.h"
#include "{{ prefixPath }}/memory/memory.h"

{% for thisModule in registermap.modules -%}
#include "{{ prefixPath }}/modules/{{ thisModule.name }}.h"
{% endfor %}

{{ registermap.name|upper }}_OPEN_EXTERN_C
{%- set registermapType = registermap.name~'_t' %}
{%- if registermap.modules|count != 0 %}

#pragma pack( {{ registermap.memory.alignment }} )

struct {{ registermapType }}
{
{%- for thisModule in registermap.modules %}
{%- set pointerType = thisModule.name~'_t volatile* const' %}
  struct {{ registermap.name }}_{{ pointerType }} {{ thisModule.name }};
{%- endfor %}
};

#pragma pack()


// Declare the register map instance for users.
extern struct {{ registermap.name }}_MemorySpace_t {{ registermap.name }}_memory;
extern struct {{ registermap.name }}_t {{ registermap.name }};
{%- endif %}

{{ registermap.name|upper }}_CLOSE_EXTERN_C

#endif
