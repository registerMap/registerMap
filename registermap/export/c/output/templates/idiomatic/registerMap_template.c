/*
 *
 * {{ registermap.name }}
 *
 {%- if licenseText is not none %}
 {%- for line in licenseText %}
 * {{ line }}
 {%- endfor %}
 {%- endif %}
 *
 */

#include "{{ prefixPath }}/macro/extern.h"
#include "{{ prefixPath }}/memory/memory.h"
#include "{{ prefixPath }}/registermap.h"


{{ registermap.name|upper }}_OPEN_EXTERN_C

#ifdef OFF_TARGET_MEMORY

struct {{ registermap.name }}_MemorySpace_t myRegisterMap_memory = {
  .allocated_memory_span = {{ memory.size }},
};

#else

{% set memoryPointerType = memory.sizeType~' volatile* const' -%}
struct {{ registermap.name }}_MemorySpace_t myRegisterMap_memory = {
  .allocated_memory_span = {{ memory.size }},
  .base = ( {{ memoryPointerType }} ) {{ memory.baseAddress }},
};

#endif

struct {{ registermap.name }}_t {{ registermap.name }} = {
{%- for thisModule in registermap.modules %}
{%- set pointerType = 'struct '~registermap.name~'_'~thisModule.name~'_t volatile* const' %}
  .{{ thisModule.name }} = ( {{ pointerType }} )( {{ registermap.name }}_memory.base + {{ thisModule.offset }} ),
{% endfor -%}
};

{{ registermap.name|upper }}_CLOSE_EXTERN_C
