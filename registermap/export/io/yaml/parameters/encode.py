#
# Copyright 2017 Russell Smiley
#
# This file is part of registermap.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import yaml


def parameter(parameterName, value):
    """
    Encode a simple parameter for yaml data.

    :param parameterName: Name of yaml parameter.
    :param value: Parameter value in a format suitable for output to yaml.
    :return: Yaml data structure.
    """
    return {parameterName: value}


# From http://stackoverflow.com/questions/18666816/using-python-to-dump-hexidecimals-into-yaml
class HexInt(int):
    pass


def hexIntRepresenter(dumper, data):
    return yaml.ScalarNode("tag:yaml.org,2002:int", hex(data))


yaml.add_representer(HexInt, hexIntRepresenter)
