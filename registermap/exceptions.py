#
# Copyright 2016 Russell Smiley
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


class ConfigurationError(Exception):
    pass


class ConstraintError(Exception):
    pass


class IntervalError(Exception):
    """General interval error."""

    pass


class ParseError(Exception):
    pass
