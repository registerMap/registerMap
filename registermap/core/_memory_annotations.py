#  Copyright (c) 2023 Russell Smiley
#
#  This file is part of registermap.
#
#  You should have received a copy of the GNU General Public License
#  along with registermap.  If not, see <http://www.gnu.org/licenses/>.

"""Core type declarations relating to memory configuration."""

import typing

AddressIndex = int
OptionalAddressIndex = typing.Optional[int]

BitIndex = int

MemoryIndex = int
OptionalMemoryIndex = typing.Optional[MemoryIndex]
