#  Copyright (c) 2023 Russell Smiley
#
#  This file is part of registermap.
#
#  You should have received a copy of the GNU General Public License
#  along with registermap.  If not, see <http://www.gnu.org/licenses/>.

from .._memory_annotations import BitIndex, MemoryIndex, OptionalMemoryIndex

DEFAULT_ADDRESS_BITS: BitIndex = 48
DEFAULT_BASE_ADDRESS: MemoryIndex = 0x0
DEFAULT_PAGE_SIZE_MEMORY_UNITS: OptionalMemoryIndex = None
DEFAULT_MEMORY_UNIT_BITS: BitIndex = 16

DEFAULT_EMPTY_TEXT = ""
