#  Copyright (c) 2023 Russell Smiley
#
#  This file is part of registermap.
#
#  You should have received a copy of the GNU General Public License
#  along with registermap.  If not, see <http://www.gnu.org/licenses/>.

"""Core register map memory configuration data model for input and output."""

import pydantic

from .._memory_annotations import BitIndex, MemoryIndex, OptionalMemoryIndex
from ._default_values import (
    DEFAULT_ADDRESS_BITS,
    DEFAULT_BASE_ADDRESS,
    DEFAULT_MEMORY_UNIT_BITS,
    DEFAULT_PAGE_SIZE_MEMORY_UNITS,
)


class MemoryConfiguration(pydantic.BaseModel):
    """Define memory configuration parameters."""

    address_bits: BitIndex = DEFAULT_ADDRESS_BITS
    base_address: MemoryIndex = DEFAULT_BASE_ADDRESS
    memory_unit_bits: BitIndex = DEFAULT_MEMORY_UNIT_BITS
    page_size: OptionalMemoryIndex = DEFAULT_PAGE_SIZE_MEMORY_UNITS
