#  Copyright (c) 2023 Russell Smiley
#
#  This file is part of registermap.
#
#  You should have received a copy of the GNU General Public License
#  along with registermap.  If not, see <http://www.gnu.org/licenses/>.

"""Core register map constraint data model for input and output."""

import pydantic

from .._memory_annotations import OptionalMemoryIndex

FixedAddressConstraint = OptionalMemoryIndex
FixedSizeConstraint = OptionalMemoryIndex
MemoryAlignmentConstraint = OptionalMemoryIndex


class ConstraintCollection(pydantic.BaseModel):
    """Define constraint types for memory addressable elements."""

    fixed_address: FixedAddressConstraint = None
    fixed_size: FixedSizeConstraint = None
    memory_alignment: MemoryAlignmentConstraint = None
