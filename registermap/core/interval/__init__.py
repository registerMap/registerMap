#
# Copyright 2017 Russell Smiley
#
# This file is part of registermap.
#
# You should have received a copy of the GNU General Public License
# along with registermap.  If not, see <http://www.gnu.org/licenses/>.
#

"""Define register map integer intervals."""

from ._contiguous import make_contiguous  # noqa: F401
from ._element import ClosedIntegerInterval  # noqa: F401
from ._overlap import any_overlap, is_encapsulated, is_overlap  # noqa: F401
from ._sort import sort_intervals  # noqa: F401
from .exceptions import IntervalError  # noqa: F401
